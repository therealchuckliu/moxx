Template.eventSuggestions.events  = {
    'click #dismissSuggestion' : function(e,t){
        Meteor.call('removeSuggestion',{
            eventId: Session.get('eventId'),
            userId: this._id
        });
    },
    'click #invite' : function(e,t){
        var usernames = [this.username];
        Meteor.call('invite',{
            eventId: Session.get('eventId'),
            usernames: usernames
            }
        );
    }

};

Template.eventSuggestions.getEventId = function(){
    return Session.get("eventId");
};