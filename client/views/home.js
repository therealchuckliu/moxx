var tSession;
var defaultCount = 5;

Template.home.created = function(){
    resetActiveNavView();
    if(tSession == undefined){
        tSession = getTemplateSession();
    }

};

Template.home.events({
    'click #loadMore' : function(evt, tmp) {
        incrementHomeCount();
    }
});

loadHome = function() {
    if(tSession == undefined){
        tSession = getTemplateSession();
    }
    tSession.set("count", defaultCount);
};

unloadHome = function() {
    tSession.set("count", undefined);
};

getHomeCount = function() {
    return tSession.get("count");
};

var incrementHomeCount = function() {
    tSession.set("count", getHomeCount() + defaultCount);
};