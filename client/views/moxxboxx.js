Template.moxxboxx.rendered = function(){
    $(document).ready(function(){
        setSize();

        $(window).resize(function(){
            setSize();
        });
    });
};

Template.moxxboxx.events({
    "keydown .chatTitle" : function(evt, tmp){
        if(evt.keyCode == 13){
            var chatTitle = tmp.find("#"+evt.target.id).value;
            if (chatTitle.length > 0){
                createChat(chatTitle, "");
            }
            tmp.find("#"+evt.target.id).value = "";
        }
    },
    "click #createChat" : function(evt, tmp){
        var chatTitle = tmp.find("#chatTitle1").value;
        if (chatTitle.length > 0){
            createChat(chatTitle, "");
        }
        tmp.find("#chatTitle1").value = "";
    },
    "click .leaveChat" : function(evt, tmp) {
        Meteor.call("leaveChat",{
            chatId: this._id
        }, function(err) {
        });
    },

    "click .chat-div" : function(e,t){
        if(e.target.id){
            Router.go('/moxxboxx/'+ e.target.id);
        }
    },

    "click #create-chat" : function(e,t){
        $('#create-chat').blur();
        $('#create-div').collapse('hide');
        $('#create-div-collapse').collapse('show');
    }
});

Template.moxxboxx.getLastMessage = function(messages){
    return messages[messages.length-1];
};

Template.moxxboxx.getLastUsername = function(memberHash, chatId, userId) {
    return memberHash[chatId][userId];
};

Template.moxxboxx.formatChatMembers = function(members, id) {
    return formatChatMembers(members[id]);
};

loadMoxxBoxx = function() {
    Session.set("moxxBoxxHash", {});
};

unloadMoxxBoxx = function() {
    Session.set("moxxBoxxHash", undefined);
};

getMoxxHash = function() {
    return Session.get("moxxBoxxHash");
};

setMoxxHash = function(hash) {
    Session.set("moxxBoxxHash", hash);
};


//data to store the list of messages to slice from mongo
chatMessageIncrement = 10;

loadChatSession = function(id) {
    Session.set("chatSession", {
        usernameHash: {},
        scrollTop: false,
        attachment: undefined,
        chatId: id
    });
    Session.set("chatMessages", chatMessageIncrement);
};

unloadChatSession = function() {
    Session.set("chatSession", undefined);
    Session.set("chatMessages", undefined);
};


getChatId = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat)
        return currentChat.chatId;
    return false;
};

getChatCount = function() {
    return Session.get("chatMessages");
};

incrementChatMessages = function(inc) {
    var count = Session.get("chatMessages") + inc;
    Session.set("chatMessages", count);
    return count;
};

setScrollTop = function(bool) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.scrollTop = bool;
        Session.set("chatSession", currentChat);
    }
};

getScrollTop = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        return currentChat.scrollTop;
    }
    return false;
};

getUsernameHash = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        return currentChat.usernameHash;
    }
    return false;
};

setUsernameHash = function(usernameHash) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.usernameHash = usernameHash;
        Session.set("chatSession", currentChat);
    }
};

getAttachment = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat && currentChat.attachment !== undefined)
        return currentChat.attachment;
    return false;
};

setAttachment = function(attachment) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.attachment = attachment;
        Session.set("chatSession", currentChat);
    }
};

createChat = function(chatTitle, eventId){
    Meteor.call("createChat",{
        title: chatTitle,
        eventId: eventId
    });
};

var setSize = function(){
    var finalHeight = Math.max(100,$(window).height() - 100);
    $('#moxxboxx').height(finalHeight);
    $('#chats').height(Math.max(0,finalHeight-$('#create').height()));
};