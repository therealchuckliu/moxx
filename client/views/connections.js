var tSession;

Template.connections.created = function(){
    tSession = getTemplateSession();
    tSession.set("active","everyone");
};



Template.connections.isActive = function(name){
    return tSession.get("active") == name ? "active" : "";
};

var consumerCount = function(obj){
    return obj.consumerData ? obj.consumerData.count() : 0;
};

Template.connections.consumerCount = function(){
    return consumerCount(this);
};

var brandCount = function(obj){
    return obj.brandData ? obj.brandData.count() : 0;
};

Template.connections.brandCount = function(){
    return brandCount(this);
};

Template.connections.everyoneCount = function(){
    return consumerCount(this) + brandCount(this);
};

Template.connections.connections = function(){
    if(tSession.get("active") == "everyone"){
        if (this.consumerData)
            this.consumerData.rewind();
        if (this.brandData)
            this.brandData.rewind();
        return (this.consumerData ? this.consumerData.fetch() : []).concat(this.brandData ? this.brandData.fetch() : []);
    }else if(tSession.get("active") == "people"){
        return this.consumerData;
    }else{
        return this.brandData;
    }
};

Template.connections.events = {
    "click #everyone" : function(){
        tSession.set("active","everyone");
    },

    "click #brands" : function(){
        tSession.set("active","brands");
    },

    "click #people" : function(){
        tSession.set("active","people");
    }
};