var tSession;
var number = 0;
var map;
Template.event.created = function(){
    tSession = getTemplateSession();
    resetActiveNavView();
};

Template.event.rendered = function(){
    $( document ).ready(function() {


        setSize();
        var lat_lng = new google.maps.LatLng(Session.get('lat'), Session.get('lng'));
        var mapOptions = {
            zoom: 15,
            center: lat_lng
        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        var marker = new google.maps.Marker({
            position: lat_lng,
            map: map
        });


        var div = $('#map-canvas');
        var width = div.width();
        div.css('height', width);

        $(window).resize(function(){
            var div = $('#map-canvas');
            var width = div.width();
            div.css('height', width);
            setSize();
        });
    });

};

Template.event.destroyed = function() {
    $(window).off('resize');
};

Template.event.events({
    "click .btn" : function(e,t){
        $('#'+ e.target.id).blur();
    },
    "keydown #comment" : function(evt, tmp){
        if(evt.keyCode == 13){
            if(tmp.find('#comment').value){
                incrementCommentCount(1);
                Meteor.call("addComment",{
                    eventId: this.eventData._id,
                    comment: tmp.find("#comment").value
                }, function (error, res) {
                    if(error){
                        alert(error);
                    }else{
                        tmp.find('#comment').value = '';
                    }
                });
            }
        }
    },

    'click #post-comment' : function(evt,tmp){
        if(tmp.find('#comment').value){
            incrementCommentCount(1);
            Meteor.call("addComment",{
                eventId: this.eventData._id,
                comment: tmp.find("#comment").value
            }, function (error, res) {
                if(error){
                    alert(error);
                }else{
                    tmp.find('#comment').value = '';
                }
            });
        }
    },

    'click #attendance' : function(e, t){
        Router.go('/attendees/'+this.eventData._id);
    },

    'click #attend' : function(e,t){
        var id = this._id;
        Meteor.call('updateRSVP',{
                eventId: id,
                rsvp : parseInt(rsvp_codes['attending'])
            }
        );
    },

    'click #unattend' : function(e,t){
        var id = this._id;
        Meteor.call('updateRSVP',{
                eventId: id,
                rsvp : parseInt(rsvp_codes['notAttending'])
            }
        );
    },

    "click .rsvp-btn" : function(e, t){
        var id = this.two['_id'];
        if(tSession.get('attending') != e.target.id){
            tSession.set('attending', e.target.id);
            Meteor.call('updateRSVP',{
                    eventId: id,
                    rsvp : parseInt(rsvp_codes[e.target.id.toString()])
                }
            );
        }
    },

    "click #like" : function(e,t){
        Meteor.call('updateLike',
            {eventId: this.eventData._id, like : !_.contains(this.eventData.like,Meteor.userId())},
            function(err, res){
                if(err){
                    alert(err);
                }
            }
        );
    },


    "click .likeComment" : function(evt, tmp){
        Meteor.call("likeComment",{
            eventId: getEventId(),
            userId: this.userId,
            timeStamp: this.timeStamp,
            addRemove: !_.contains(this.likes, Meteor.userId())
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    },

    'click .userpic' : function(e,t){
        var owner = Meteor.users.findOne({_id: this.userId});
        Router.go('/profile/'+owner.username);
    },

    "click .removeComment" : function(evt, tmp){
        Meteor.call("removeComment",{
            eventId: getEventId(),
            timeStamp: this.timeStamp
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    },

    'click #addPollTime' : function(evt, tmp){
        Meteor.call("addPollTime",{
            eventId: this._id,
            pollTime: tmp.find("#pollTime").value
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
        tmp.find("#pollTime").value = "";
    },

    'click .checkPollTime' : function(evt, tmp){
        Meteor.call("choosePollTime",{
            eventId: getEventId(),
            pollTime: this.time,
            isChecked: evt.target.checked
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    },

    'click .deletePollTime' : function(evt, tmp){
        Meteor.call("deletePollTime",{
            eventId: getEventId(),
            pollTime: this.time
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    },

    'click #loadEarlier' : function(evt, tmp) {
        incrementCommentCount(defaultCommentCount);
    },

    'keydown .invite-input' : function(e,t){
        if(e.keyCode == 13){
            var val = t.find('#'+ e.target.id).value;
            if(val.length > 0){
                inviteUsers(this.eventData._id,val);
                t.find('#'+ e.target.id).value = '';
            }
        }
    },
    'click #invite-users' : function(e,t){
        var val = t.find('#invite-input1').value;
        if(val.length > 0){
            inviteUsers(this.eventData._id,val);
            t.find('#'+ e.target.id).value = '';
        }
    },
    'click #invite' : function(e,t){
        tSession.set('showInvite',!tSession.get('showInvite'));
    }
});

var inviteUsers = function(id, users){
    var usernames = users.replace(/ /g,'').split(",");
    Meteor.call('invite',{
        eventId: id,
        usernames: usernames
    },function(err,res){
        if(!err){
            tSession.set('showInvite',false);
        }else{
            alert(err);
        }
    });
};


Template.event.getNumAttendees = function(){
    return this.eventData.attending.length;
};

Template.event.getAttendingString = function(){
    return _.contains(this.eventData.attending,Meteor.userId()) ? strings['attending'] : strings['notAttending'];
};

Template.event.getLikeString = function(array){
    return _.contains(array,Meteor.userId()) ? strings['unlike'] : strings['like'];
};

Template.event.getCommentData = function(){
    return {eventId: this.eventData._id, commentData: this.eventData.comments};
};

Template.event.getCommentOwnerPhoto = function(){
    var owner = Meteor.users.findOne({_id: this.userId});
    var length = owner.profile.uploadedpic.length;
    return owner.profile.uploadedpic[length-1].src;
};

Template.event.isMyComment = function(userId){
    return userId == Meteor.userId();
};

Template.event.hasComments = function(eventData){
    return eventData.comments.length > 0;
}

Template.event.getNewNumber = function(){
    return number++;
};

Template.event.isChecked = function() {
    return _.contains(this.users, Meteor.userId()) ? "checked" : "";
};

Template.event.showInvite = function(){
    return tSession.get('showInvite') == true;
};

var setSize = function(){
    var windowHeight = $(window).height();
    var topDivHeight = $('#top-div').height();
    var remainHeight = Math.max(100,windowHeight - topDivHeight - 265);
    $('.thread').height(remainHeight);
};

var defaultCommentCount = 5;
loadEvent = function(id) {
    Session.set("commentCount", defaultCommentCount);
    Session.set("eventId", id);
};

setLatLong = function(lat,lng){
    Session.set('lat',lat);
    Session.set('lng',lng);
};

getEventId = function() {
    return Session.get("eventId");
};

getCommentCount = function() {
    return Session.get("commentCount");
};

var incrementCommentCount = function(count) {
    Session.set("commentCount", getCommentCount() + count);
};

Template.event.viewSuggestions = function(){
    return (this.myEvent && _.size(this.eventData.suggested) > 0);
};

Template.event.getThis = function(){
    alert(JSON.stringify(this, null, 4));
};

Template.event.hasLocation = function(eventData){
    return eventData.location ? true : false;
};

Template.event.getRSVPBtnType = function(name){
    return _.size(this.two[name]) > 0 ? (_.contains(this.two[name],Meteor.userId()) ? 'btn-info' : 'btn-default') : 'btn-default';
};

Template.event.amIAttending = function(){
    return _.contains(this.attending,Meteor.userId());
};

Template.event.amIInvited = function(){
    return _.contains(this.invited, Meteor.userId());
};



Template.event.rsvpOptions = function(){
    return _.contains(this.eventData.invited, Meteor.userId()) ? rsvp_code_strings_invited : rsvp_code_strings_public;
};

Template.event.inviteOrSuggest = function(){
    return (this.myEvent || this.eventData.open) ? strings['inviteToEvent'] : strings['suggestInvitation'];
};


Template.event.inviteOrSuggestPrompt = function(){
    return ((this.myEvent || this.eventData.open) ? strings['inviteToEvent'] :
        strings['suggestInvitation'])+' '+strings['addUsersToEventSuffixPrompt'];
};

unloadEvent = function() {
    Session.set("commentCount", undefined);
    Session.set("eventId", undefined);
};