var tSession;
var mainHeight;
Template.main.created = function(){
    tSession = getTemplateSession();
};

Template.main.rendered = function(){
    $(document).ready(function(){
        $('#moxx').css({
            'position' : 'absolute',
            'left' : '50%',
            'top'  : '50%',
            'margin-left' : -$('#moxx').width()/2,
            'margin-top' : -$('#moxx').height()/2
        }) ;
        $('#text').css({
            'width' : $('#moxx').width(),
            'position': 'absolute',
            'left' : '50%',
            'top'  : '50%',
            'margin-left' : -$('#moxx').width()/2,
            'margin-top': $('#moxx').height()/2
        });
        window.setTimeout(function(){
            $('#text').collapse();
            },
        500);
    });
};

Template.main.destroyed = function(){
    $(window).off('resize');
};

Template.main.getTemplateSession = function(){
    return tSession;
};

//code for main page
Template.main.getRegistrationForm = function(){
    return tSession.get("business-registration") ? "business-registration-form" : "registration-form";
};

Template.main.showBusinessRegistrationForm = function(){
    return tSession.get("business-registration");
};

Template.main.events({

    'click #business-registration' : function(){
        tSession.set("business-registration", true);
    },

    'click #personal-registration' : function(){
        tSession.set('business-registration', false);
    },

    'click #register' : function(evt,tmp){
        if (tSession.get("business-registration")) {
            //get user inputs
            var name = tmp.find("#businessRegFirstLast").value;
            var bname = tmp.find("#businessCompanyName").value;
            var url = tmp.find("#businessWebsite").value;
            var phone = tmp.find("#businessPhone").value.replace(/[^0-9]/g, '');
            var address = tmp.find("#businessAddress").value;
            var email = tmp.find("#businessRegEmail").value;
            var pw1 = tmp.find("#businessRegPasswordOne").value;

            if(tmp.find('#businessAddress').value.length > 0){
                geocodeAddress(tmp.find('#businessAddress').value, function(lat_lng){
                    if(_.size(lat_lng) > 0){
                        createBrand(name, bname, url, phone, address, email, pw1, pw1, [lat_lng['lng'], lat_lng['lat']])
                    }else{
                        createBrand(name, bname, url, phone, address, email, pw1, pw1,[]);
                    }
                });
            }else{
                createBrand(name, bname, url, phone, address, email, pw1, pw1,[]);
            }

        }
        else {
            //get user inputs
            var name = tmp.find("#name").value;
            var email = tmp.find("#email").value;
            var pw1 = tmp.find("#reg-password").value;
            createConsumer(name, email, pw1, pw1);
        }
    },

    "keydown #password" : function(e,t){
        if(e.keyCode == 13){
            t.find('#login').click();
        }
    },

    "keydown #businessRegPasswordOne" : function(e,t){
        if(e.keyCode == 13){
            t.find('#register').click();
        }
    },

    "keydown #reg-password" : function(e,t){
        if(e.keyCode == 13){
            t.find('#register').click();
        }
    },

    "click #login" : function(e,t){
        tryLogin(t.find('#loginId').value, t.find('#password').value);
    },

    'click #business-registration' : function(){
        tSession.set("business-registration", true);
    },
    'click #personal-registration' : function(){
        tSession.set('business-registration', false);
    },
    'click #register' : function(evt, tmp) {
        if (tSession.get("business-registration") == true) {
            //get user inputs
            var name = tmp.find("#businessRegFirstLast").value;
            var bname = tmp.find("#businessCompanyName").value;
            var url = tmp.find("#businessWebsite").value;
            var phone = tmp.find("#businessPhone").value.replace(/[^0-9]/g, '');
            var address = tmp.find("#businessAddress").value;
            var email = tmp.find("#businessRegEmail").value;
            var pw1 = tmp.find("#businessRegPasswordOne").value;


            if(tmp.find('#businessAddress').value.length > 0){
                geocodeAddress(tmp.find('#businessAddress').value, function(lat_lng){
                    if(_.size(lat_lng) > 0){
                        createBrand(name, bname, url, phone, address, email, pw1, pw1, [lat_lng['lng'], lat_lng['lat']])
                    }else{
                        createBrand(name, bname, url, phone, address, email, pw1, pw1,[]);
                    }
                });
            }else{
                createBrand(name, bname, url, phone, address, email, pw1, pw1,[]);
            }

        }
        else {
            //get user inputs
            var name = tmp.find("#name").value;
            var email = tmp.find("#email").value;
            var pw1 = tmp.find("#reg-password").value;
            createConsumer(name, email, pw1, pw1);
        }
    }
});

Template.register.showBusinessRegistrationForm = function(){
    return tSession.get("business-registration");
};

var createBrand = function(name,business_name,website,phoneNumber,address,email, pw1, pw2, lng_lat) {
    //parse to get first and last names
    var nameArr = name.trim().split(" ");
    if (nameArr.length <= 1 || name.replace(/[a-z\s]+$/ig, "").length > 0) {
        Template.popup.showPopupMessage("Invalid Name", "You must enter both first and last name");
    }
    else if (business_name.length == 0) {
        Template.popup.showPopupMessage("Invalid Business Name", "You must enter a valid business name");
    }
    else if (phoneNumber.length != 10) {
        Template.popup.showPopupMessage("Invalid Number", "Enter a valid phone number, including area code");
    }
    else if (address.length == 0) {
        Template.popup.showPopupMessage("Invalid Address", "Enter a valid address");
    }
    else if (!validEmail(email)) {
        Template.popup.showPopupMessage("Invalid Email", "You must enter a valid email");
    }
    else if (pw1 != pw2 || pw1.replace(/[a-z0-9]+$/ig, "") != "" || pw1.length == 0) {
        Template.popup.showPopupMessage("Invalid Password", "Passwords must match each other and contain only alphanumeric values");
    }
    else {
        var last_name = nameArr[nameArr.length-1];
        var first_name = "";
        var i;
        for (i = 0 ; i < nameArr.length-1 ; i++) {
            first_name = first_name + nameArr[i];
        }

        Meteor.call('createBrand', {
            first_name: first_name,
            last_name: last_name,
            business_name: business_name,
            website: website,
            phoneNumber: phoneNumber,
            address: address,
            email : email,
            password : pw1,
            location : lng_lat
        }, function (error, venue) {
            if (! error) {
                tryLogin(email, pw1);
            }
            else {
                tryLogout();
                if (Meteor.user())
                    Meteor.call('deleteUser', {hasBrand: Meteor.user().profile.hasBrand});
                Template.popup.showPopupMessage("Register Failed",
                    error.reason || "Could not create user");
            }
        });
    }
};

var createConsumer = function(name,email, pw1, pw2) {

    //parse to get first and last names
    var nameArr = name.trim().split(" ");
    if (nameArr.length <= 1 ||  name.replace(/[a-z\s]+$/ig, "").length > 0) {
        Template.popup.showPopupMessage("Invalid Name", "You must enter both first and last name");
    }
    else if (!validEmail(email)) {
        Template.popup.showPopupMessage("Invalid Email", "You must enter a valid email");
    }
    else if (pw1 != pw2 || pw1.replace(/[a-z0-9]+$/ig, "") != "" || pw1.length == 0) {
        Template.popup.showPopupMessage("Invalid Password", "Passwords must match each other and contain only alphanumeric values");
    }
    else {
        var last_name = nameArr[nameArr.length-1];
        var first_name = "";
        var i;
        for (i = 0 ; i < nameArr.length-1 ; i++) {
            first_name = first_name + nameArr[i];
        }
        Meteor.call('createConsumer', {
            first_name: first_name,
            last_name: last_name,
            email : email,
            password : pw1
        }, function (error, consumer) {
            if (! error) {
                tryLogin(email, pw1);
            }
            else {
                tryLogout();
                if (Meteor.user())
                    Meteor.call('deleteUser', {hasBrand: Meteor.user().profile.hasBrand});
                Template.popup.showPopupMessage("Register Failed",
                    error.reason || "Could not create user");
            }
        });
    }
};