var tSession;

Template.categories.created = function(){
    tSession = getTemplateSession();
    loadCategories();
};

Template.categories.events = {
    'click .category' : function(event, template){
        updateCategory(event.target.id);
        $('#'+event.target.id).blur();
    }
};

Template.categories.selectString = function(name, trueString, falseString){
        return stringSelector(name, tSession, trueString, falseString);
};

loadCategories = function(){
    var myCategories = Meteor.user().profile.categories;
    for (var category in myCategories) {
        tSession.set(myCategories[category], true);
    }
};

updateCategory = function(category){
    var remove = tSession.isDefined(category) ? tSession.get(category) : false;
    tSession.set(category, !remove);
    Meteor.call('updateCategory', {
        category : category,
        removeFromDB : remove
    },
    function(err, res){
        if(err){
            tSession.set(category, remove);
            Template.popup.showPopupMessage("error", err.reason);
        }
    });
};