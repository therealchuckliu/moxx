var tSession;
Template.profile.created = function(){
    if (tSession == undefined)
        tSession = getTemplateSession();
};
var defaultCount = 5;

Template.profile.rendered = function(){
    $(document).ready(function(){
        setSize();

        $(window).resize(function(){
            setSize();
        });
    });
};


Template.profile.destroyed = function() {
    $(window).off('resize');
};

Template.profile.hasCategories = function(){
    return this.profile.categories.length > 0;
};

Template.profile.events({

    "click .connection" : function(e,t){
        $('#'+ e.target.id).blur();
        var brandProfile = isBrandProfile(this);
        var followRequestSent = brandProfile ? _.contains(Meteor.user().brandReqSent, this._id)
            : _.contains(Meteor.user().consumerReqSent, this._id);
        var doIFollowUser = this.profile.hasBrand ? _.contains(Meteor.user().brandFollowing, this._id) :
            _.contains(Meteor.user().consumerFollowing, this._id);
        var methodToCall = (doIFollowUser || brandProfile)  ? 'updateMyFollowing' : 'updateFollowingRequest';
        var addOrRemove = doIFollowUser ? false : brandProfile ? true : !followRequestSent;
        Meteor.call(methodToCall, {id: this._id, addRemove: addOrRemove});
    },

    "click #approveFollowRequest" : function(){
        approveFollowRequest(this.userInfo._id);
    },

    "click #ignoreFollowRequest" : function(){
        ignoreFollowRequest(this.userInfo._id);
    },
//    "click #pic" : function(){
//        tSession.set("profPicIndex", this.profile.uploadedpic.length - 1);
//        tSession.set("profPicArr", this.profile.uploadedpic);
//        Template.popup.showPopupMessage(this.profile.uploadedpic.length - tSession.get("profPicIndex") + " of " + this.profile.uploadedpic.length,
//            getProfPopupHTML(this.profile.uploadedpic,tSession.get("profPicIndex")));
//    },

    'click #addPic' : function(event, tmpl){
        filepicker.pick({
                mimetypes: ['image/*', 'text/plain'],
                container: 'window',
                services:['COMPUTER', 'URL']
            },
            function(InkBlob){
                filepicker.convert(InkBlob, {width: 500, height: 500},
                    function(new_InkBlob){
                        uploadPic(new_InkBlob.url);
                    },
                    function(err){
                        uploadPic(InkBlob.url);
                    }
                );
            },
            function(FPError){
                console.log(FPError.toString());
            }
        );
    },

    'click #loadMore' : function(evt, tmp) {
        incrementCount();
    }
});

var uploadPic = function(url){
    Meteor.call("addProfPic",{
        uploadedpic : url,
        caption : ""
    },function(err,res){
        if(err){
            alert(err);
        }
    });
};

var getProfPopupHTML = function(uploadedpic, index) {
    var showNext = index > 0 ? '<button class="btn btn-primary" id="nextprofpic">Next</button>' : '';
    var showPrev = index < uploadedpic.length - 1 ? '<button class="btn btn-primary" id="prevprofpic">Prev</button>' : '';
    return '<div class="row text-center">'
        + showPrev
        + '<img id="imgsrc" height="400px" width="400px" src="' + uploadedpic[index].src + '">'
        + showNext
        + '</div>' +
        '<div class="row text-center" id="imgcaption">' +
            uploadedpic[index].caption
        + '</div>';
};

var setSize = function(){
    var windowHeight = $(window).height();
    var topDivHeight = $('#event-top-div').height();
    var remainHeight = Math.max(50,windowHeight - topDivHeight - 130);
    $('#event-list').height(remainHeight);
};

var isBrandProfile = function(ctx){
    return ctx.profile.hasBrand ? true : false;
};

var doIFollowUser = function(ctx){
    return ctx.profile.hasBrand ? _.contains(Meteor.user().brandFollowing, ctx._id) :
        _.contains(Meteor.user().consumerFollowing, ctx._id);
};

var hasFollowRequestBeenSent = function(ctx){
    return isBrandProfile(ctx) ? _.contains(Meteor.user().brandReqSent, ctx._id)
        : _.contains(Meteor.user().consumerReqSent, ctx._id);
};

Template.profile.doIFollowUser = function(){
    return doIFollowUser(this);
};


Template.profile.haveIRequestedToFollow = function(){
    return hasFollowRequestBeenSent(this);
};

Template.profile.showViewRequests = function(){
    return this.myProfile && (((Meteor.user().consumerReqReceived != undefined) && _.size(Meteor.user().consumerReqReceived) > 0)
        || ((Meteor.user().brandReqReceived != undefined) && _.size(Meteor.user().brandReqReceived) > 0));
};

Template.profile.followString = function(){
    return getFollowString(this.userInfo,tSession);
};

Template.profile.getColumnSize = function(){
    return this.myProfile ? 'col-sm-6' : showConnections(this) ? 'col-sm-4' : hasRequestedToFollowMe(this.userInfo) ? 'col-sm-4' : 'col-xs-12 text-center';
};

Template.profile.attachUsername = function(){
    return this.userInfo.username;
};

Template.profile.hasRequestedToFollowMe = function(){
    return hasRequestedToFollowMe(this.userInfo);
};

Template.profile.getNoEventsString = function(){
    if(this.myProfile){
        return strings['noEvents'];
    }else{
        return (this.userInfo.profile.hasBrand ? this.userInfo.profile.business_name : this.userInfo.profile.first_name)+" "+strings['userNoEvents'];
    }
};

var showConnections = function(ctx){
    var followerArray;
    if(Meteor.user().profile.hasBrand){
        followerArray = ctx.userInfo.brandFollowers;
    }else{
        followerArray = ctx.userInfo.consumerFollowers;
    }
    return  ctx.myProfile || (followerArray ? _.contains(followerArray,Meteor.userId()) : false);
};

Template.profile.showConnections = function(){
    return showConnections(this);
};

Template.profile.eventString = function() {
    var name = this.profile.hasBrand ? this.profile.business_name : this.profile.first_name;
    return (name.substring(name.length-2, name.length) == "'s" ? name.substring(0,name.length-2) : name) + "'s Events";
};

loadProf = function(username) {
    if (tSession == undefined)
        tSession = getTemplateSession();
    tSession.set("count", defaultCount);
    tSession.set("username", username);
};

unloadProf = function() {
    tSession.set("count", undefined);
};

getProfUsername = function() {
    return tSession.get("username");
};

getProfCount = function() {
    return tSession.get("count");
};

var incrementCount = function() {
    tSession.set("count", defaultCount + getProfCount());
};