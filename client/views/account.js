//functions for account page
var tSession;
//array that holds ids of text fields that have text in them
var nonEmptyElements;
var currentlyExpandedId;
Template.account.created = function(){
    tSession = getTemplateSession();
    nonEmptyElements = [];
};

Template.account.getSessionObject = function(){
    return tSession;
};

Template.account.getEnabled = function (name) {
        return isEnabled(name, tSession);
};

Template.account.selectString = function (name, trueString, falseString) {
        return stringSelector(name, tSession, trueString, falseString);
};

Template.account.getUsernameBtnString = function(){
    return constantsStringSelector("usernameAvailable", tSession,'available','notAvailable');
};

Template.account.showParagraph = function(){
    return tSession.get('username');
};

Template.account.usernameValid = function(){
    return tSession.get('usernameValid');
};

var typingTimer;
var doneTypingInterval = 500;//delay calling checkEmpty in milliseconds

Template.account.events = {
    "keyup .checkEmpty" : function(event){
        typingTimer = Meteor.setTimeout(function() {checkEmpty(event)}, doneTypingInterval);
   },

    "click #changePwd" : function(e,t){
        $('#changePwd').blur();
        if(currentlyExpandedId != undefined){
            $(currentlyExpandedId).collapse('hide');
            $(currentlyExpandedId.replace('-div-collapse','-div')).collapse('show');
        }
        $('#pwd-div').collapse('hide');
        $('#pwd-div-collapse').collapse('show');
        currentlyExpandedId = '#pwd-div-collapse';

    },

    "click #changePwd-cancel" : function(e,t){
        $('#pwd-div-collapse').collapse('hide');
        $('#pwd-div').collapse('show');
        currentlyExpandedId = undefined;
    },

    "click .change" : function(e,t){
        $('#'+ e.target.id).blur();

        if(currentlyExpandedId != undefined){
            $(currentlyExpandedId).collapse('hide');
            $(currentlyExpandedId.replace('-div-collapse','-div')).collapse('show');
        }
        $('#'+ e.target.id.replace('-change','-div')).collapse('hide');
        $('#'+ e.target.id.replace('-change','-div-collapse')).collapse('show');
        currentlyExpandedId = '#'+ e.target.id.replace('-change','-div-collapse');
   },

    "click .cancel" : function(e,t){
        $('#'+ e.target.id.replace('-change-cancel',"-div-collapse")).collapse('hide');
        $('#'+ e.target.id.replace('-change-cancel',"-div")).collapse('show');
        currentlyExpandedId = undefined;
    },

    "keydown .checkEmpty" : function(event){
        Meteor.clearTimeout(typingTimer);
    },

    "click .btn" : function(e,t){
        $('#'+event.target.id).blur();
    },

    "click #name" : function(e,t){
        $('#name-div').on('shown.bs.collapse', function () {
            Meteor.call('updateInfo',
                {
                    field: 'first_name',
                    value: t.find("#first_name-new").value
                }
            );
            Meteor.call('updateInfo',
                {
                    field: 'last_name',
                    value: t.find("#last_name-new").value
                }
            );
        });
        $('#name-div-collapse').collapse('hide');
        $('#name-div').collapse('show');
        currentlyExpandedId = undefined;
    },

    "click #email" : function(e,t){
        if(!validEmail(t.find("#email-new").value)){
            Template.popup.showPopupMessage("Error", "Invalid Email");
            e.stopPropagation();
        }else{
            $('#email-div').on('shown.bs.collapse',function(){
                Meteor.call('updateInfo',
                    {
                        field: 'email',
                        value: t.find("#email-new").value
                    }
                );

            });
            $('#email-div').collapse('show');
            $('#email-div-collapse').collapse('hide');
            currentlyExpandedId = undefined;
        }
    },

    "click #username" : function(e,t){
        $('#username-div').on('shown.bs.collapse',function(){
            Meteor.call('updateInfo',
                {
                    field: 'username',
                    value: t.find("#username-new").value
                }
            );
        });
        $('#username-div').collapse('show');
        $('#username-div-collapse').collapse('hide');
        currentlyExpandedId = undefined;
    },

    "click #business_name" : function(e,t){
        $('#bn-name-div').on('shown.bs.collapse',function(){
            var val = t.find('#business_name-new').value;
            if(val.length > 0){
                Meteor.call('updateInfo',
                    {
                        field: 'business_name',
                        value: val
                    }
                );
            }
        });
        $('#bn-name-div').collapse('show');
        $('#bn-name-div-collapse').collapse('hide');
        currentlyExpandedId = undefined;

    },

    "click #phoneNumber" : function(e,t){

        $('#bn-contact-div').on('shown.bs.collapse',function(){
            var val = t.find('#phoneNumber-new').value;
            if(val.length > 0){
                Meteor.call('updateInfo',
                    {
                        field: 'phoneNumber',
                        value: val
                    }
                );
            }
        });
        $('#bn-contact-div').collapse('show');
        $('#bn-contact-div-collapse').collapse('hide');
        currentlyExpandedId = undefined;
    },

    "click #address" : function(e,t){
        var val = t.find('#address-new').value;
        if(val.length > 0){
            Meteor.call('updateInfo',
                {
                    field: 'address',
                    value: val
                }
            );
        }
        $('#bn-address-change').animate({opacity: 100},function(){
            $(this).css('pointer-events', 'auto');
        });
    },

    "click #website" : function(e,t){
        $('#bn-website-div').on('shown.bs.collapse',function(){
            var val = t.find('#website-new').value;
            if(val.length > 0){
                Meteor.call('updateInfo',
                    {
                        field: 'website',
                        value: val
                    }
                );
            }
        });
        $('#bn-website-div').collapse('show');
        $('#bn-website-div-collapse').collapse('hide');
        currentlyExpandedId = undefined;

    },
    "click #update-pwd" : function(e,t){
        var oldPassword = t.find('#oldPass').value;
        var newPassOne = t.find('#pass1').value;
        var newPassTwo = t.find('#pass2').value;

        if(newPassOne === newPassTwo){
            Accounts.changePassword(oldPassword, newPassOne, function(err){
                if(err){
                    Template.popup.showPopupMessage("Incorrect password", "Please try again");
                }else{
                    $('#pwd-div').collapse('show');
                    $('#pwd-div-collapse').collapse('hide');
                    currentlyExpandedId = undefined;
                }
            })
        }else{
            Template.popup.showPopupMessage("Passwords do not match", "Please try again");
        }
    }
};

updateInfo = function(tmp, fields){
    for(var index in fields){
        var field = fields[index];
        var value = tmp.find("#" + field).value;
        if (field.toLowerCase().indexOf("email") != -1 && !validEmail(value)) {
            resetField(tmp, field);
            Template.popup.showPopupMessage("Error", "Invalid Email");
        }
        else{
            Meteor.call('updateInfo',
                {
                    field: field,
                    value: tmp.find("#" + field).value
                },
                function(err, res) {
                    if (err)
                        Template.popup.showPopupMessage("Error", err.reason);
                    else
                        resetField(tmp, res.field);
                }
            );
        }
    }
};

//Functions to emulate a set by only allowing one unique element in the array
checkEmpty = function(event){
    if($('#'+event.target.id).val().length>0){
        addElement(event.target.id);
        if(event.target.id == 'username'){
            checkAvailability($('#username').val(), tSession, 'usernameValid');
        }

    }else{
        removeElement(event.target.id);
        if(event.target.id == 'username'){
            tSession.set('usernameValid',false);
        }

    }
};

removeElement = function(name){
    var index = nonEmptyElements.indexOf(name);
    if(index > -1){
        nonEmptyElements.splice(index,1);
    }
    tSession.set(name, false);
    checkUpdateAll();
};


addElement = function(name){
    var index = nonEmptyElements.indexOf(name);
    if(index > -1){
        nonEmptyElements.splice(index,1);
    }
    tSession.set(name, true);
    nonEmptyElements.push(name);
    checkUpdateAll();
};

checkUpdateAll = function(){
    tSession.set("updateAll",(nonEmptyElements.length > 0));
};

var resetField = function(tmp, field) {
    if(field.length > 0){
        tmp.find("#" + field).value = "";
        tSession.set(field, false);
        removeElement(field);
    }
};



