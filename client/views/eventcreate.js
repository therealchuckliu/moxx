var tSession;
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var startdate, enddate;
Template.eventcreate.created = function(){
    tSession = getTemplateSession();
    setStartValues(tSession);
};

Template.eventcreate.rendered = function(){
    startdate = $('#create-start-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > enddate.date.valueOf()) {
                var newDate = new Date(ev.date);
                enddate.setValue(newDate);
            }else{
                enddate.setValue(enddate.date); //hacker to trigger rendering/disable
            }
            startdate.setValue(ev.date);
            startdate.hide();
        }).data('datepicker');
    enddate = $('#create-end-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < startdate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            enddate.hide();
        }).data('datepicker');
};

Template.eventcreate.events({
    "click .category" : function(e,t){
        tSession.set(e.target.id,tSession.get(e.target.id) ? false : true);
        $('#'+ e.target.id).blur();
    },
    "click .end-minute" : function(e,t){
        var old = tSession.get('active-end-minute');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-end-minute', e.target.id);
    },

    "click .event-attr" : function(e,t){
        tSession.set(e.target.id,tSession.get(e.target.id) ? false : true);
    },

    "click #create-event" : function(e,t){
        if(t.find('#event-address').value.length > 0){
            geocodeAddress(t.find('#event-address').value, function(lat_lng){
                _.size(lat_lng) > 0 ? createEvent(e,t,[lat_lng['lng'], lat_lng['lat']]) : createEvent(e,t,[]);
            });
        }else{
            createEvent(e,t,[]);
        }
    },

    "click .category" : function(e,t){
        $('#'+ e.target.id).blur();
    },
    "click .start-meridiem" : function(e,t){
        tSession.set('active-start-meridiem', e.target.id);
    },

    "click .end-meridiem" : function(e,t){
        tSession.set('active-end-meridiem', e.target.id);
    },

    "click .end-hour" : function(e,t){
        var old = tSession.get('active-end-hour');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-end-hour', e.target.id);
    },

    "click .start-minute" : function(e,t){
        var old = tSession.get('active-start-minute');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-start-minute', e.target.id);
    },

    "click .start-hour" : function(e,t){
        var old = tSession.get('active-start-hour');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-start-hour', e.target.id);
    },

    "click .day" : function(e,t){
        tSession.set(e.target.id, tSession.get(e.target.id) ? false : true);
    },

    'click #login-button': function(e, tmpl) {
        tryLogin(getUsername(tmpl), getPassword(tmpl));
    },

    'click #login-email' : function(e, tmpl) {
        e.stopPropagation();
    },

    'click #photo' : function(event, template){
        filepicker.pick({
                mimetypes: ['image/*', 'text/plain'],
                container: 'window',
                services:['COMPUTER', 'URL']
            },
            function(InkBlob){
                setConvertedImage(InkBlob, tSession);
            },
            function(FPError){
                console.log(FPError.toString());
            }
        );
    },

    'click #amorpm' : function(e,t){
        tSession.set('am', tSession.get('am') ? false : true);
    },

    'click #amorpm-e' : function(e,t){
        tSession.set('am-e', tSession.get('am-e') ? false : true);
    }
});

var setStartValues = function(tSession){
    var current = new Date();
    var currentHour = parseInt(current.getHours());
    var pm = currentHour > 11;
    currentHour %= 12;
    currentHour = (currentHour == 0) ? 12 : currentHour;
    var currentMinutes = parseInt(current.getMinutes());
    currentMinutes -= currentMinutes%5;
    var singleDigit = currentMinutes < 10;

    var startString = currentMinutes.toString()+"-m";
    var endString = currentMinutes.toString()+"-me";

    if(singleDigit){
        startString = '0'+startString;
        endString = '0'+endString;
    }

    tSession.set('active-start-minute',startString);
    tSession.set('active-end-minute',endString);
    tSession.set(startString,true);
    tSession.set(endString, true);

    var startHourString = currentHour.toString()+'-h';
    var endHourString = currentHour.toString()+'-he';
    tSession.set('active-start-hour',startHourString);
    tSession.set('active-end-hour',endHourString);
    tSession.set(startHourString,true);
    tSession.set(endHourString, true);

    if(!pm){
        tSession.set('active-start-meridiem','am');
        tSession.set('am',true);
        tSession.set('active-end-meridiem','am-e');
        tSession.set('am-e',true);
    }else{
        tSession.set('active-start-meridiem','pm');
        tSession.set('am',false);
        tSession.set('active-end-meridiem','pm-e');
        tSession.set('am-e',false);
    }

};

var getEndDateObj = function(tSession){
    var endDateObj = new Date(enddate.date);
    var endHour = parseInt(tSession.get('active-end-hour').substring(0,2));
    var endMinutes = parseInt(tSession.get('active-end-minute').substring(0,2));
    var pm = tSession.get('active-end-meridiem') == 'pm-e';
    if(pm){
        endHour = 12+(endHour % 12);
    }else{
        endHour %= 12;
    }

    endDateObj.setHours(endHour);
    endDateObj.setMinutes(endMinutes);
    return endDateObj;

};

var getStartDateObj = function(tSession){
    var startDateObj = new Date(startdate.date);
    var startHour = parseInt(tSession.get('active-start-hour').substring(0,2));
    var startMinutes = parseInt(tSession.get('active-start-minute').substring(0,2));
    var pm = tSession.get('active-start-meridiem') == 'pm';
    if(pm){
        startHour = 12+(startHour % 12);
    }else{
        startHour %= 12;
    }

    startDateObj.setHours(startHour);
    startDateObj.setMinutes(startMinutes);
    return startDateObj;
};

var createEvent = function(e,t,lng_lat){
        eventCategories = [];

        var startDateObj, endDateObj;
        for(name in e_categories){
            if(tSession.get(name)){
                eventCategories.push(name);
            }
        }

        if(tSession.get('recurring') == true){
            startDateObj = new Date();
            var dayBefore = new Date();
            dayBefore.setDate(startDateObj.getDate() - 1);
            endDateObj = dayBefore;
        }else{
            startDateObj = getStartDateObj(tSession);
            endDateObj = getEndDateObj(tSession);
        }

        var privateEvent = tSession.get('private') == true;
        var recurring = tSession.get('recurring') == true;

        Meteor.call("createEvent", {
            title: t.find('#event-title').value,
            tagline: t.find('#event-tagline').value,
            description: t.find('#event-description').value,
            uploadedpic: t.find('#photo').src,
            timeStart: startDateObj,
            timeEnd: endDateObj,
            address: t.find('#event-address').value,
            location: lng_lat,
            open: !privateEvent,
            categories: eventCategories,
            recurring: recurring,
            recString: generateRecString()
        }, function(err, res){
            if(err){
                alert(err);
            }else{
                Router.go("/event/" + res);
            }
        });
};

var generateRecString = function() {
    var recString = "";
    for (var d in event_days) {
        recString += tSession.get(d) ? "1" : "0";
    }
    return recString;
};

Template.eventcreate.isActive = function(id){
    return tSession.get(id) ? 'active' : '';
};

Template.eventcreate.showWarning = function(){
    return tSession.get('title');
};

Template.eventcreate.selectString = function(name, trueString, falseString){
    return stringSelector(name, tSession, trueString, falseString);
};

Template.eventcreate.showEndDateTime = function(){
    return tSession.get('enddatetime');
};

Template.eventcreate.getEndDateTimeString = function(){
    var enterQuery, removeQuery;
    if(tSession.get('recurring')){
        enterQuery = strings['enterEndTimeQuery'];
        removeQuery = strings['removeEndTimeQuery'];
    }else{
        enterQuery = strings['enterEndDateQuery'];
        removeQuery = strings['removeEndDateQuery'];
    }
    return tSession.get('enddatetime') ? removeQuery : enterQuery;
};

Template.eventcreate.isEndDateTimeHidden = function(){
    return tSession.get('enddatetime') ? '' : 'hidden';
};

Template.eventcreate.isEnabled = function(name){
    return tSession.get(name) ? '' : 'disabled';
};

Template.eventcreate.getImageSource = function(){
    return getImageSourceFromTSession(tSession,'image');
};

Template.eventcreate.recurring = function() {
    return tSession.get('recurring') ? true : false;
};

Template.eventcreate.getDate = function(){
    return (now.getMonth()+1).toString()+'/'+now.getDate().toString()+'/'+now.getFullYear().toString();
};

Template.eventcreate.amorpm = function(){
    return tSession.get("am") ? 'am' : 'pm';
};

Template.eventcreate.amorpmval = function(){
    return tSession.get("am") ? 'pm' : 'am';
}

Template.eventcreate.amorpme = function(){
    return tSession.get("am-e") ? 'am' : 'pm';
};

Template.eventcreate.amorpmvale = function(){
    return tSession.get("am-e") ? 'pm' : 'am';
};

Template.eventcreate.isEventRecurring = function(){
    return tSession.get('recurring');
};

Template.eventcreate.isDayActive = function(day){
    return tSession.get(day) ? "active" : "";
};

Template.eventcreate.isActiveStartMeridiem = function(id){
    return tSession.get('active-start-meridiem') === id ? 'active' : '';
};

Template.eventcreate.isActiveEndMeridiem = function(id){
    return tSession.get('active-end-meridiem') === id ? 'active' : '';
};

Template.eventcreate.days = function(){
    return event_days;
};