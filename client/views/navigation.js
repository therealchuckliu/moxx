var tSession;
var startdate, enddate;
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
Template.navigation.created = function(){
    tSession = getTemplateSession();
    tSession.set('active-start-minute','00-m');
    tSession.set('active-start-hour', '12-h');
    tSession.set('active-end-minute','00-me');
    tSession.set('active-end-hour','12-he');

    tSession.set('00-m',true);
    tSession.set('12-h',true);
    tSession.set('00-me',true);
    tSession.set('12-he',true);
};

Template.navigation.rendered = function(){
    startdate = $('#create-start-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > enddate.date.valueOf()) {
                var newDate = new Date(ev.date);
                enddate.setValue(newDate);
            }else{
                enddate.setValue(enddate.date); //hacker to trigger rendering/disable
            }
            startdate.setValue(ev.date);
            startdate.hide();
        }).data('datepicker');
    enddate = $('#create-end-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < startdate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            enddate.hide();
        }).data('datepicker');
};

Template.navigation.events({
    "click .category" : function(e,t){
        tSession.set(e.target.id,tSession.get(e.target.id) ? false : true);
        $('#'+ e.target.id).blur();
    },
    "click .end-minute" : function(e,t){
        var old = tSession.get('active-end-minute');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-end-minute', e.target.id);
    },

    "click #create-event" : function(e,t){
        if(t.find('#event-address').value.length > 0){
            geocodeAddress(t.find('#event-address').value, function(lat_lng){
                _.size(lat_lng) > 0 ? createEvent(e,t,[lat_lng['lng'], lat_lng['lat']]) : createEvent(e,t,[]);
            });
        }else{
            createEvent(e,t,[]);
        }
    },

    "click #recurring" : function(e,t){
        tSession.set('recurring',tSession.get('recurring') ? false : true);
    },

    "click .end-hour" : function(e,t){
        var old = tSession.get('active-end-hour');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-end-hour', e.target.id);
    },

    "click .start-minute" : function(e,t){
        var old = tSession.get('active-start-minute');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-start-minute', e.target.id);
    },

    "click .start-hour" : function(e,t){
        var old = tSession.get('active-start-hour');
        if(old){
            tSession.set(old,false);
        }
        tSession.set(e.target.id,true);
        tSession.set('active-start-hour', e.target.id);
    },

    "click .day" : function(e,t){
        tSession.set(e.target.id, tSession.get(e.target.id) ? false : true);
    },

    'click #login-button': function(e, tmpl) {
        tryLogin(getUsername(tmpl), getPassword(tmpl));
    },

    'click #login-email' : function(e, tmpl) {
        e.stopPropagation();
    },

    'click #photo' : function(event, template){
        filepicker.pick({
                mimetypes: ['image/*', 'text/plain'],
                container: 'window',
                services:['COMPUTER', 'URL']
            },
            function(InkBlob){
                setConvertedImage(InkBlob, tSession);
            },
            function(FPError){
                console.log(FPError.toString());
            }
        );
    },

    'click #amorpm' : function(e,t){
        tSession.set('am', tSession.get('am') ? false : true);
    },

    'click #amorpm-e' : function(e,t){
        tSession.set('am-e', tSession.get('am-e') ? false : true);
    },

    'click #login-password' : function(e, tmpl) {
        e.stopPropagation();
    },

    'click .logout-button' : function(e, tmpl) {
        tryLogout();
    },

    'keyup .loginInput' : function(e, tmpl){
        if(e.keyCode == 13){
            tryLogin(getUsername(tmpl), getPassword(tmpl));
        }
    }
});

Template.navigation.selectString = function(name, trueString, falseString){
    return stringSelector(name, tSession, trueString, falseString);
};

Template.navigation.getTemplateSession = function(){
    return tSession;
};

Template.navigation.isHourActive = function(id){
    return tSession.get(id) ? "active" : "";
};


Template.navigation.isMinuteActive = function(id){
    return tSession.get(id) ? "active" : "";
};

Template.navigation.validUser = function() {
    return (Meteor.user() && Meteor.user().username) ? Meteor.user().username : "";
};

Template.navigation.failedAuthentication = function(){
    return tSession.get("failedAuth")
};

Template.navigation.getDate = function(){
    return (now.getMonth()+1).toString()+'/'+now.getDate().toString()+'/'+now.getFullYear().toString();
};

Template.navigation.amorpm = function(){
    return tSession.get("am") ? 'am' : 'pm';
};

Template.navigation.amorpmval = function(){
    return tSession.get("am") ? 'pm' : 'am';
}

Template.navigation.amorpme = function(){
    return tSession.get("am-e") ? 'am' : 'pm';
};

Template.navigation.getImageSource = function(){
    return getImageSourceFromTSession(tSession,'image');
};

Template.navigation.amorpmvale = function(){
    return tSession.get("am-e") ? 'pm' : 'am';
};

Template.navigation.isEventRecurring = function(){
    return tSession.get('recurring');
};

Template.navigation.isDayActive = function(day){
    return tSession.get(day) ? "active" : "";
}

getUsername = function(tmpl){
    return tmpl.find("#login-email").value;
};

getPassword = function(tmpl){
    return tmpl.find("#login-password").value;
};

var createEvent = function(e,t,lng_lat){
    var eventCategories = [];

    for(name in e_categories_xs){
        if(tSession.get(name)){
            eventCategories.push(name);
        }
    }
    var startDateObj, endDateObj;
    if(!tSession.get('recurring')){
        startDateObj = new Date(startdate.date);
        endDateObj = new Date(enddate.date);
    }else{
        startDateObj = new Date();
        endDateObj = new Date();
    }

    var startadditional = tSession.get('am') ? 12 : 0;
    var startmod = startadditional + 12;
    var startHour = parseInt(tSession.get('active-start-hour').substring(0,2));
    startHour = startHour % 12;
    var startMinutes = parseInt(tSession.get('active-start-minute').substring(0,2));
    startHour = ((startHour + startadditional)%startmod);
    startDateObj.setMinutes(startMinutes);
    startDateObj.setHours(startHour);

    var endadditional = tSession.get('am-e') ? 12 : 0;
    var endmod = endadditional + 12;
    var endHour = parseInt(tSession.get('active-end-hour').substring(0,2));
    var endMinutes = parseInt(tSession.get('active-end-minute').substring(0,2));
    endHour = endHour % 12;
    endHour = ((endHour + endadditional)%endmod);
    endDateObj.setHours(endHour);
    endDateObj.setMinutes(endMinutes);

//    if(!tSession.get('recurring') && !tSession.get('enddatetime')){
//        var dayBefore = new Date();
//        dayBefore.setDate(startDateObj.getDate() - 1);
//        endDateObj = dayBefore;
//    }

    Meteor.call("createEvent", {
        title: t.find('#event-title').value,
        tagline: t.find('#event-tagline').value,
        description: t.find('#event-description').value,
        uploadedpic: t.find('#photo').src,
        timeStart: startDateObj,
        timeEnd: endDateObj,
        address: t.find('#event-address').value,
        location: lng_lat,
        open: !t.find('#private-event').checked,
        categories: eventCategories,
        recurring: tSession.get('recurring') ? true : false,
        recString: generateRecString()
    }, function(err, res){
        if(err){
            alert(err);
        }else{
            $('#event-create-modal').modal('hide');
            resetFields();
            Router.go("/event/" + res);
        }
    });
};

Template.navigation.days = function(){
    return event_days;
};

var generateRecString = function() {
    var recString = "";
    for (var d in event_days) {
        recString += tSession.get(d) ? "1" : "0";
    }
    return recString;
};