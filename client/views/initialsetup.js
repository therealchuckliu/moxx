var tSession;
Template.initialsetup.created = function(){
    tSession = getTemplateSession();
};

var typingTimer;
var doneTypingInterval = 500;//delay calling checkEmpty in milliseconds

Template.initialsetup.events = {
    "keyup .checkEmpty" : function(event, tmp){
        if($('#'+event.target.id).val().length>0){

            var text = tmp.find('#acctUserName').value;
            typingTimer = Meteor.setTimeout(
                function() {
                    if(!tSession.get(event.target.id)){
                        tSession.set(event.target.id, true);
                    }
                    checkAvailability(text, tSession, 'usernameAvail')
                }, doneTypingInterval);
        }else{
            tSession.set(event.target.id, false);
        }
    },

    "keydown .checkEmpty" : function(event){
        if($('#'+event.target.id).val().length == 0){
            tSession.set(event.target.id, false);
        }
        Meteor.clearTimeout(typingTimer);
    },

    "click #attachment": function(event, template){
        filepicker.pick({
                mimetypes: ['image/*', 'text/plain'],
                container: 'window',
                services:['COMPUTER', 'URL']
            },
            function(InkBlob){
                setConvertedImage(InkBlob, tSession);
            },
            function(FPError){
                console.log(FPError.toString());
            }
        );
    },

    "click #completeReg": function(evt, tmp) {
        completeRegistration(tmp.find("#acctUserName").value, tmp.find("#uploadedpic").src, "");
    }
};


var completeRegistration = function(username, uploadedpic, caption) {
    if (username.replace(/[a-z0-9]+$/ig, "") != "" || username.length == 0) {
        Template.popup.showPopupMessage("Invalid Username", "You must enter a username with only alphanumeric characters");
    }
    else {
        Meteor.call('completeRegistration', {
            username: username,
            uploadedpic: uploadedpic,
            caption: caption
        }, function (error, res) {
            if (error)  {
                Template.popup.showPopupMessage("Final Registration Failed",
                    error.reason || "Could not complete registration");
            }
        });
    }
};

Template.initialsetup.getEnabled = function (name) {
    return isEnabled(name, tSession);
};

Template.initialsetup.selectString = function (name, trueString, falseString) {
    return stringSelector(name, tSession, trueString, falseString);
};

Template.initialsetup.selectConstantString = function(name, trueString, falseString){
    return constantsStringSelector(name, tSession, trueString, falseString);
};

Template.initialsetup.triModeSelector = function(name, nonVerifiedString, verifiedString, defaultString ){
    return triButtonModeSelector(name, tSession, nonVerifiedString, verifiedString, defaultString);
};

Template.initialsetup.showParagraph = function(){
    return tSession.get('acctUserName') ? true : false;
};

Template.initialsetup.getParagraphClass = function(){
    if(tSession.get('usernameAvail')){
        return 'green-text';
    }
    return 'red-text';
};

Template.initialsetup.getImageSource = function(){
    return getImageSourceFromTSession(tSession);
};
