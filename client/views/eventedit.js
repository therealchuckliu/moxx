var tSession;
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var startdate, enddate;
Template.eventedit.created = function(){
    tSession = getTemplateSession();
};

Template.eventedit.rendered = function(){
    startdate = $('#startdate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > enddate.date.valueOf()) {
                var newDate = new Date(ev.date);
                enddate.setValue(newDate);
            }else{
                enddate.setValue(enddate.date); //hacker to trigger rendering/disable
            }
            startdate.setValue(ev.date);
            startdate.hide();
        }).data('datepicker');
    enddate = $('#enddate').datepicker({
        onRender: function(date) {
            return date.valueOf() < startdate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            enddate.hide();
        }).data('datepicker');
};

Template.eventedit.events = {
    'keyup #title' : function(evt, tmpl){
        tSession.set(evt.target.id,($('#title').val().length>0));
    },
    'blur #startdate' : function(e,t)
    {
        if(t.find('#startdate').value.length == 0){
            startdate.setValue(startdate.date);
            startdate.hide();
        }
    },
    'blur #enddate' : function(e,t){
        if(t.find('#enddate').value.length == 0){
            enddate.setValue(startdate.date);
            enddate.hide();
        }
    },
    'click #photo' : function(event, template){
        filepicker.pick({
                mimetypes: ['image/*', 'text/plain'],
                container: 'window',
                services:['COMPUTER', 'URL']
            },
            function(InkBlob){
                setConvertedImage(InkBlob, tSession);
            },
            function(FPError){
                console.log(FPError.toString());
            }
        );
    },
    'click #end-date-toggle' : function(){
        tSession.get('enddatetime') ? tSession.set('enddatetime',false) : tSession.set('enddatetime', true);
    },
    'click #update' : function(e, t){
        changedCategories = [];

        for(name in e_categories){
            if(_.contains(this.eventData.categories,name) != tSession.get(name)){
                changedCategories[name] = !tSession.get(name);
            }
        }

        for(name in changedCategories){
            Meteor.call("updateEventCategory",{
                eventId: this.eventData._id,
                category: name,
                removeFromDB: changedCategories[name]
            });
        }

        if(t.find('#photo').src != this.eventData.uploadedpic){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'uploadedpic',
                value: t.find('#photo').src
            });
        }

        if(t.find('#title').value.length > 0){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'title',
                value: t.find('#title').value
            })
        }

        if(t.find('#tagline').value.length > 0){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'tagline',
                value: t.find('#tagline').value
            })
        }

        if(t.find('#description').value.length > 0){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'description',
                value: t.find('#description').value
            })
        }

        if(t.find('#address').value.length > 0){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'address',
                value: t.find('#address').value
            })
        }

        if(this.eventData.recurring){
            var recString = generateRecString();
            if(this.eventData.recurring !== recString){
                Meteor.call('updateEventInfo',{
                    eventId: this.eventData._id,
                    field: 'recurring',
                    value: recString
                })
            }
        }

        var startDateObj, endDateObj;
        if(this.eventData.recurring){
            startDateObj = new Date(startdate.date);
            endDateObj = new Date(enddate.date);
        }else{
            startDateObj = new Date();
            endDateObj = new Date();
        }
        var startadditional = t.find('#starttime-ampm').value == 'am' ? 0 : 12;
        var startmod = startadditional + 12;
        var startHour = parseInt(t.find('#starttime-hours').value);
        startHour = startHour % 12;
        var startMinutes = parseInt(t.find('#starttime-minutes').value);
        startHour = ((startHour + startadditional)%startmod);
        startDateObj.setMinutes(startMinutes);
        startDateObj.setHours(startHour);

        var endadditional = t.find('#endtime-ampm').value == 'am' ? 0 : 12;
        var endmod = endadditional + 12;
        var endMinutes = parseInt(t.find('#endtime-minutes').value);
        var endHour = parseInt(t.find('#endtime-hours').value);
        endHour = endHour % 12;
        endHour = ((endHour + endadditional)%endmod);
        endDateObj.setHours(endHour);
        endDateObj.setMinutes(endMinutes);

        if(this.eventData.recurring && !tSession.get('enddatetime')){
            var dayBefore = new Date();
            dayBefore.setDate(startDateObj.getDate() - 1);
            endDateObj = dayBefore;
        }

        if(this.eventData.recurring){
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'HHMMStart',
                value: dateToHHMM(startDateObj)
            });
            if(tSession.get('enddatetime')){
                Meteor.call('updateEventInfo',{
                    eventId: this.eventData._id,
                    field: 'HHMMEnd',
                    value: dateToHHMM(endDateObj)
                });
            }
        }else{
            Meteor.call('updateEventInfo',{
                eventId: this.eventData._id,
                field: 'timeStart',
                value: startDateObj
            });
            if(tSession.get('enddatetime')){
                Meteor.call('updateEventInfo',{
                    eventId: this.eventData._id,
                    field: 'timeEnd',
                    value: endDateObj
                });
            }
        }

        Meteor.call('updateEventInfo',{
            eventId: this.eventData._id,
            field: 'open',
            value: !t.find('#privateEvent').checked
        });

        Router.go("/event/"+this.eventData._id);
    },

    'click .category' : function(evt, tmpl){
        var category = evt.target.id;
        tSession.set(category, !(tSession.get(category)));
    },

    'click .recurrDay' : function(evt, tmpl){
        var day = evt.target.id;
        tSession.set(day, !(tSession.get(day)));
    },

    "click #date" : function(){
    },

    "click #recurring" : function() {
        tSession.set("recurring", !tSession.get("recurring"));
    }
};

var generateRecString = function() {
    var recString = "";
    for (var d in days) {
        recString += tSession.get(d) ? "1" : "0";
    }
    return recString;
};

Template.eventedit.getEventPicSource = function(){
    if(tSession.get('imagesource') == undefined){
        return this.eventData.uploadedpic;
    }
    return tSession.get('imagesource');
};

Template.eventedit.showWarning = function(){
    return tSession.get('title');
};

Template.eventedit.recurringSelector = function(name, trueString, falseString, days){
    var index = parseInt(name);
    if(tSession.get(name) == undefined){
        tSession.set(name, days.charAt(index) == '1');
    }
    return tSession.get(name) == true ? trueString : falseString;
};

Template.eventedit.categorySelector = function(name, trueString ,falseString, categories){
    if(tSession.get(name) == undefined){
        tSession.set(name,_.contains(categories,name));
    }
    return tSession.get(name) == true ? trueString : falseString;
};

Template.eventedit.isPrivateEventChecked = function(){
    if(tSession.get('privateEvent') == undefined){
        tSession.set('privateEvent',!this.eventData.open);
    }
    return tSession.get('privateEvent') == true ? 'checked' : '';
};

Template.eventedit.isRecurringEventChecked = function(){
    return tSession.get('recurring') ? 'checked' : '';
};

Template.eventedit.selectString = function(name, trueString, falseString){
    return stringSelector(name, tSession, trueString, falseString);
};

Template.eventedit.showEndDateTime = function(){
    return tSession.get('enddatetime');
};

Template.eventedit.getEndDateTimeString = function(){
    return tSession.get('enddatetime') ? strings['removeEndDateTimeQuery'] : strings['enterEndDateTimeQuery'];
};

Template.eventedit.getColumnsForTime = function(){
    return this.eventData.recurring ? 'col-md-4 col-md-offset-4' : 'col-md-5';
};

Template.eventedit.getEndDateTimeString = function(){
    var enterQuery, removeQuery;
    if(this.eventData.recurring){
        enterQuery = strings['enterEndTimeQuery'];
        removeQuery = strings['removeEndTimeQuery'];
    }else{
        enterQuery = strings['enterEndDateQuery'];
        removeQuery = strings['removeEndDateQuery'];
    }
    return tSession.get('enddatetime') ? removeQuery : enterQuery;
};

Template.eventedit.isEndDateTimeHidden = function(){
    if(tSession.get('enddatetime') == undefined){
        if(this.eventData.recurring){
            tSession.set('enddatetime',this.eventData.HHMMEnd ? true : false);
        }else{
            tSession.set('enddatetime',this.eventData.timeEnd ? true : false);
        }
    }
    return tSession.get('enddatetime') ? '' : 'hidden';
};

Template.eventedit.isEnabled = function(name){
    return tSession.get(name) ? '' : 'disabled';
};

Template.eventedit.recurring = function() {
    return this.eventData.recurring ? true : false;
};

Template.eventedit.getStartDate = function(){
    if(!this.eventData.recurring){
        var startDate = new Date(this.eventData.timeStart);
        return (startDate.getMonth()+1).toString()+'/'+startDate.getDate().toString()+'/'+startDate.getFullYear().toString();
    }
};

Template.eventedit.getStartHour = function(){
    var start, hour;
    if(this.eventData.recurring){
        start = parseInt(this.eventData.HHMMStart);
        hour = (start/100) % 12;
    }else{
        start = new Date(this.eventData.timeStart);
        hour = parseInt(start.getHours()) % 12;
    }
    return hour == 0 ? 12 : hour;
};

Template.eventedit.getStartMinute = function(){
    var start,minutes;
    if(this.eventData.recurring){
        start = parseInt(this.eventData.HHMMStart);
        minutes = '0'+(start % 100).toString();
    }else{
        start = new Date(this.eventData.timeStart);
        minutes = '0'+(start.getMinutes()).toString();
    }
    return minutes.substring(minutes.length-2, minutes.length);
};

Template.eventedit.getStartPeriod = function(){
    var start, hour;
    if(this.eventData.recurring){
        start = parseInt(this.eventData.HHMMStart);
        hour = start/100;
    }else{
        start = new Date(this.eventData.timeStart);
        hour = parseInt(start.getHours());
    }
    return hour > 11 ? 'am' : 'pm';
};

Template.eventedit.getEndHour = function(){

};

Template.eventedit.getEndMinute = function(){

};

Template.eventedit.getEndPeriod = function(){

};

Template.eventedit.getDate = function(){
    return (now.getMonth()+1).toString()+'/'+now.getDate().toString()+'/'+now.getFullYear().toString();
};
