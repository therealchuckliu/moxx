Template.requestsitem.rendered = function(){
    $('#ignore').tooltip({
        animation: true,
        trigger: 'hover focus',
        title: 'Ignore Follow Request',
        placement : 'auto',
        container: 'body'
    });
};

Template.requestsitem.events = {
    "click .requests-item" : function(e,t){
        Router.go('/profile/'+ this.username)
    },
    "click #approve" : function(e,t){
        approveFollowRequest(this._id);
    },
    "click #ignore" : function(e,t){
        ignoreFollowRequest(this._id);
    }
};