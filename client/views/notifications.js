Template.notifications.rendered = function() {
    Meteor.call("lastSeenNotification"
        , function (error, res) {
        if(error){
            alert(error);
        }
    });
};

defaultNotificationCount = 5;

var notificationTitles = {
    1 : '<strong>Follow Request Accepted: </strong>',
    2 : '<strong>Follow Request: </strong>',
    3 : '<strong>Event Updated: </strong>',
    4 : '<strong>Event Invite: </strong>',
    5 : '<strong>Event RSVP: </strong>',
    6 : '<strong>Event Like: </strong>',
    7 : '<strong>Event Comment: </strong>',
    8 : '<strong>Liked Comment: </strong>',
    9 : '<strong>Chat Invite: </strong>',
    10: '<strong>Event Time Check: </strong>',
    11: '<strong>Event Time Check: </strong>',
    12: '<strong>Event Time Check: </strong>'
};

var notificationStrings = {
    1 : function(options) {return "You are now following " + groupUsers(options) + "."},
    2 : function(options) {return groupUsers(options) + " requested to follow you."},
    3 : function(options) {return groupUsers(options) + " updated the details for " + getEventNameFromId(options.eventId) + "."},
    4 : function(options) {return groupUsers(options) + " invited you to his event: '" + getEventNameFromId(options.eventId) + "'."},
    5 : function(options) {return groupUsers(options) + " rsvp'ed to " + getEventNameFromId(options.eventId) + "."},
    6 : function(options) {return groupUsers(options) + " liked " + getEventNameFromId(options.eventId) + "."},
    7 : function(options) {return groupUsers(options) + " commented on event: '" + getEventNameFromId(options.eventId) + "'."},
    8 : function(options) {return groupUsers(options) + " liked your comment on " + getEventNameFromId(options.eventId) + "."},
    9 : function(options) {return groupUsers(options) + " has invited you to a chat: '" + getChatNameFromId(options.chatId) + "'."},
    10: function(options) {return groupUsers(options) + " has added a potential event time for " + getEventNameFromId(options.eventId) + "."},
    11: function(options) {return groupUsers(options) + " has removed a potential event time for " + getEventNameFromId(options.eventId) + ". If" +
                                                        " you still plan on attending please review what times work for you."},
    12: function(options) {return groupUsers(options) + " updated available times for " + getEventNameFromId(options.eventId) + "."}
};

var groupUsers = function(options) {
    if (options.users.length == 1) {
        return getUsernameFromId(options.users[0]);
    }
    else if (options.users.length == 2) {
        return getUsernameFromId(options.users[0]) + " and " + getUsernameFromId(options.users[1]);
    }
    else {
        return getUsernameFromId(options.users[options.users.length-1])
            + ", " + getUsernameFromId(options.users[options.users.length-2])
            + ", and " + (options.users.length - 2) + " other users";
    }
};

Template.notifications.getNotificationString = function() {
    return new Handlebars.SafeString(notificationTitles[this.notificationType] + notificationStrings[this.notificationType]({
        users: this.users,
        eventId: this.eventId ? this.eventId : false,
        chatId: this.chatId ? this.chatId : false
    }));
};

Template.notifications.events({
    "click .btn" : function(e,t){
        $('#'+ e.target.id).blur();
    },

    "click .deleteNotification" : function(e){
        e.stopPropagation();
        Meteor.call("deleteNotification",{
            notificationParams: this
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    },

    "click #loadEarlier" : function() {
        var count = Session.get("notificationCount");
        count = count ? count + defaultNotificationCount : defaultNotificationCount*2;
        Session.set("notificationCount", count);
    },

    "click .notification-image" : function(){
        Router.go(this.link);
    }
});

Template.notifications.getFirstUserImage = function(){
   return getUserImageFromId(this.users[0]);
};


Template.notifications.sortedNotifications = function() {
    return Meteor.user().notifications.sort(function(a,b) {return b.timeStamp - a.timeStamp});
};

getNotificationCount = function() {
    return Session.get("notificationCount");
};

resetNotifications = function() {
    Session.set("notificationCount", defaultNotificationCount);
};