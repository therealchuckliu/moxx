var tSession;

Template.thread.created = function(){
    tSession = getTemplateSession();
};

Template.thread.convertMessage = function(message) {
    return new Handlebars.SafeString(convertMessage(message));
};