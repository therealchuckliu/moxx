var tSession;

Template.admin.isVerified = function() {
    if (Meteor.user())
        return Meteor.user().emails[0].verified;
    else
        return false;
};

Template.admin.created = function(){
    if (!Template.admin.isVerified())
        Meteor.call("adminVerification", function(err, res) {
            if (err)
                alert(err);
        });

    if (tSession == undefined) {
        defaultAdmin()
    }
};

Template.admin.events({
   'click #initLoad': function(evt, tmp) {
       Meteor.call("loadInit", function (error, res) {
           if(error){
               alert(error);
           }
       });
   },

    'click .checkUser': function(evt, tmp) {
        var arr = tSession.get("users");
        if (evt.target.checked)
            arr[this.username] = this.emails[0].address;
        else
            delete arr[this.username];
        tSession.set("users", arr);
    },

    'click #checkAll' : function(evt, tmp) {
        var arr = tSession.get("users");
        var inputs = document.getElementsByTagName("input");
        for (var i = 0 ; i < inputs.length ; i++) {
            if (inputs[i].type == "checkbox" && inputs[i].parentNode.getElementsByTagName("a").length > 0) {
                if (inputs[i].checked != evt.target.checked)
                    inputs[i].click();
            }
        }
    },

    'click #emailButton' : function(evt,tmp) {
        tSession.set("showEmail", true);
    },

    'click #closeEmail' : function(evt, tmp) {
        tSession.set("showEmail", false);
    },

    'click #sendEmail' : function(evt, tmp) {
        var emails = Template.admin.toEmail().split(";");
        Meteor.call('sendAdminEmail', {to:[],bcc:emails, subject:tmp.find("#emailSubject").value, body:convertMessage(tmp.find("#emailBody").value)},
        function(err,res) {
            if(err)
                alert(err)
        });
        tSession.set("showEmail", false);
    }
});

Template.admin.showEmail = function() {
    if (tSession == undefined)
        defaultAdmin();
    return tSession.get("showEmail");
};

Template.admin.toEmail = function() {
    if (tSession == undefined) {
        defaultAdmin();
    }
    var arr = tSession.get("users");
    var str = "";
    for (var username in arr) {
        str = str + arr[username] + ";";
    }
    return str;
};

var defaultAdmin = function() {
    if (tSession == undefined)
        tSession = getTemplateSession();
    tSession.set("users", {});
    tSession.set("showEmail", false);
};