//handle popup message, no buttons in the dialog just text

Template.popup.showPopupMessage = function(popupTitle, popupMessage) {
    Session.set("popupMessage", popupMessage);
    Session.set("popupTitle", popupTitle);
    Session.set("showMyModal", true);
};

Template.popup.helpers({
    showMyModal:function(){
        return Session.get("showMyModal");
    }
});

Template.myModal.events({
    "click .close":function(){
        Session.set("showMyModal",false);
    }
});