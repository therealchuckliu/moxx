Template.resetPassword.events = {
    'click #reset-password' : function(e,t){
        var passOne = t.find('#newpassone').value;
        var passTwo = t.find('#newpasstwo').value;


        if((passOne.length > 0) && (passOne === passTwo)){
            Accounts.resetPassword(this.token,passOne,function(e){
                if(e){
                    alert(e);
                }else{
                    Router.go('/');
                }
            })
        }else{
            alert('passwords do not match');
        }
    }
};