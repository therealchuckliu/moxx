Template.useritem.rendered = function(){
    $('#removeFollower').tooltip({
        animation: true,
        trigger: 'hover focus',
        title: 'Remove Follower',
        placement : 'auto',
        container: 'body'
    });
};

Template.useritem.events = {
    "click .user-item" : function(e,t){
        Router.go('/profile/'+ this.username)
    },

    "click .connection" : function(e,t){
        $('#'+ e.target.id).blur();
        var brandProfile = isBrandProfile(this);
        var followRequestSent = brandProfile ? _.contains(Meteor.user().brandReqSent, this._id)
            : _.contains(Meteor.user().consumerReqSent, this._id);
        var doIFollowUser = this.profile.hasBrand ? _.contains(Meteor.user().brandFollowing, this._id) :
            _.contains(Meteor.user().consumerFollowing, this._id);
        var methodToCall = (doIFollowUser || brandProfile)  ? 'updateMyFollowing' : 'updateFollowingRequest';
        var addOrRemove = doIFollowUser ? false : brandProfile ? true : !followRequestSent;

        Meteor.call(methodToCall, {id: this._id, addRemove: addOrRemove});
    },

    "click #removeFollower" : function(){
        removeFollower(this._id);
    }
};

var doIFollowUser = function(ctx){
    return ctx.profile.hasBrand ? _.contains(Meteor.user().brandFollowing, ctx._id) :
        _.contains(Meteor.user().consumerFollowing, ctx._id);
};

var isUserFollowingMe = function(ctx){
    return ctx.profile.hasBrand ? _.contains(Meteor.user().brandFollowers, ctx._id) :
        _.contains(Meteor.user().consumerFollowers, ctx._id);
};

var isBrandProfile = function(ctx){
    return ctx.profile.hasBrand ? true : false;
};

var hasFollowRequestBeenSent = function(ctx){
    return isBrandProfile(ctx) ? _.contains(Meteor.user().brandReqSent, ctx._id)
        : _.contains(Meteor.user().consumerReqSent, ctx._id);
};

Template.useritem.doIFollowUser = function(){
    return doIFollowUser(this);
};

Template.useritem.haveIRequestedToFollow = function(){
    return hasFollowRequestBeenSent(this);
};



Template.useritem.isUserFollowingMe = function(){
    return isUserFollowingMe(this);
};

Template.useritem.getFolloweeString = function(){
    return hasRequestedToFollowMe(this) ? strings['approveFollowRequest'] : strings['removeFollower'];
};

Template.useritem.getFollowString = function(){
    return getFollowString(this);
};

Template.useritem.isMyUserItem = function(){
    return _.isEqual(this._id, Meteor.userId());
};