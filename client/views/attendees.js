var tSession;

Template.attendees.created = function(){
    tSession = getTemplateSession();
    tSession.set('active','attending');
};

Template.attendees.events({
    'click .active-toggle' : function(evt){
        tSession.set('active', evt.target.id);
    },
    'click #attending' : function(){
        tSession.set('active', 'attending')
    },
    'click #maybe' : function(){
        tSession.set('active','maybe')
    },
    'click #invited' : function(){
        tSession.set('active','invited')
    }
});

Template.attendees.isActive = function(name){
    return tSession.get('active') == name ? 'active' : '';
};

Template.attendees.rsvp = function(){
    if(tSession.get('active') == 'attending'){
        return this.attendees;
    }else if(tSession.get('active') == 'invited'){
        return this.invited;
    }else if(tSession.get('active') == 'maybe'){
        return this.maybeAttending;
    }else{
        return this.notAttending;
    }
};

Template.attendees.attendingCount = function(){
    return this.attendees.count();
};

Template.attendees.invitedCount = function(){
    return this.invited.count();
};

Template.attendees.maybeAttendingCount = function(){
    return this.maybeAttending.count();
};