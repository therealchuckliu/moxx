var tSession;

Template.chat.created = function(){
    tSession = getTemplateSession();
};

Template.chat.rendered = function() {
    setSize();
    $(window).resize(function() {
        setSize();
    });

    //if an <a> element was created that matches an image url ie someone posted an image, we should render that image
    //first we need to check that it's a valid image, and then we set it's innerHTML to an <img>
    var els = document.getElementsByTagName("a");
    var regExp = /[^ ]*\.(jpeg|jpg|gif|png)[ ]*/;
    for (var i = 0, l = els.length; i < l ; i++) {
        var el = els[i];
        var match = el.href.match(regExp);
        if (match && el.innerHTML.indexOf("<img") < 0) {
            checkImageAndResize(el);
        }
    }

    if (!getScrollTop()) {
        $('.thread').scrollTop($('.thread')[0].scrollHeight);
        $("#messagePlaceholder").focus();
    }
};

var checkImageAndResize = function(el){
    $("<img/>").attr("src", el.href).load(function(){
        if (this.width) {
            var ren = Meteor.render('<img height = "300" width = "' + 300*this.width/this.height + '" src="' + el.href + '" border="0">');
            el.replaceChild(ren, el.firstChild);
        }
    });
};

Template.chat.destroyed = function() {
    $(window).off('resize');
};

Template.chat.events({
    'click #postImage' : function(event, tmpl){
        var chatId = this._id;
        if (!getAttachment())
            filepicker.pick({
                    mimetypes: ['image/*', 'text/plain'],
                    container: 'window',
                    services:['COMPUTER']
                },
                function(InkBlob){
                    filepicker.stat(InkBlob, {width: true, height: true},
                        function(metadata){
                            var width = metadata["width"];
                            var height = metadata["height"];
                            setAttachment({src: InkBlob.url,
                                height: height < 300 ? height : 300,
                                width: height < 300 ? width : 300 * width/height});
                        });
                },
                function(FPError){
                    console.log(FPError.toString());
                }
            );
        else
            setAttachment(undefined);
    },
    "click #addMembers" : function(evt, tmp){
        var members = tmp.find("#addMemberList").value.replace(/ /g,'').split(",");
        if (members.length > 0)
            Meteor.call("addMember",{
                chatId: this._id,
                members: members
            }, function (error, res) {
                if(error){
                    alert(error);
                }
            });
        tmp.find("#addMemberList").value = "";
    },

    "keydown #messagePlaceholder" : function(evt, tmp){
        if(evt.keyCode == 13){
            evt.preventDefault();
            postMessage(this._id,tmp);
        }
    },

    "keydown .add-members-input" : function(e,t){
        if(e.keyCode == 13){
            e.preventDefault();
            var members = e.target.value.replace(/ /g,'').split(",");
            if (members.length > 0)
                Meteor.call("addMember",{
                    chatId: this._id,
                    members: members
                }, function (error, res) {
                    if(error){
                        alert(error);
                    }
                }
            );
            e.target.value="";
        }
    },
    "click #postMessage" : function(evt, tmp){
        evt.preventDefault();
        postMessage(this._id, tmp);
    },

    "click #loadEarlier" : function(evt, tmp){
        evt.preventDefault();
        setScrollTop(true);
        var count = incrementChatMessages(chatMessageIncrement);
    },

    "click .leaveChat" : function(evt, tmp) {
        Router.go('/moxxboxx');
        Meteor.call("leaveChat",{
            chatId: this._id
        },function (err) {
            if (err) {
                alert(err);
            }
        });
    }
});

var postMessage = function(id, tmp){
    setScrollTop(false);
    var message = tmp.find("#messagePlaceholder").value;
    var attachment = getAttachment();
    if (message.length > 0 || attachment)
        Meteor.call("writeMessage",{
            chatId: id,
            message: message,
            upload: attachment
        }, function (error, res) {
            if(error){
                alert(error);
            }
        });
    setAttachment(undefined);
    incrementChatMessages(1);
    tmp.find("#messagePlaceholder").value = "";
};

Template.chat.uploadImageIcon = function() {
    return getAttachment() ? "btn-danger" : "btn-primary";
};

Template.chat.attachmentString = function() {
    return getAttachment() ? strings["removeAttachment"] : strings["addAttachment"];
};

var setSize = function(){
    var windowHeight = $(window).height();
    var postBoxHeight = $('.thread-post').height();
    var threadHeaderHeight = $('#thread-header').height();
    var remainHeight = Math.max(70,windowHeight - postBoxHeight - threadHeaderHeight - 150);
    $('.thread').height(remainHeight);
};

//data to store the list of messages to slice from mongo
chatMessageIncrement = 10;

loadChatSession = function(id) {
    Session.set("chatSession", {
        usernameHash: {},
        scrollTop: false,
        attachment: undefined,
        chatId: id
    });
    Session.set("chatMessages", chatMessageIncrement);
};

unloadChatSession = function() {
    Session.set("chatSession", undefined);
    Session.set("chatMessages", undefined);
};

getChatId = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat)
        return currentChat.chatId;
    return false;
};

getChatCount = function() {
    return Session.get("chatMessages");
};

incrementChatMessages = function(inc) {
    var count = Session.get("chatMessages") + inc;
    Session.set("chatMessages", count);
    return count;
};

setScrollTop = function(bool) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.scrollTop = bool;
        Session.set("chatSession", currentChat);
    }
};

getScrollTop = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        return currentChat.scrollTop;
    }
    return false;
};

getUsernameHash = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        return currentChat.usernameHash;
    }
    return false;
};

setUsernameHash = function(usernameHash) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.usernameHash = usernameHash;
        Session.set("chatSession", currentChat);
    }
};

getAttachment = function() {
    var currentChat = Session.get("chatSession");
    if (currentChat && currentChat.attachment !== undefined)
        return currentChat.attachment;
    return false;
};

setAttachment = function(attachment) {
    var currentChat = Session.get("chatSession");
    if (currentChat) {
        currentChat.attachment = attachment;
        Session.set("chatSession", currentChat);
    }
};