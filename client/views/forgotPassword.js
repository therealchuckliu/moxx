Template.forgotPassword.events = {
    'click #send' : function(e,t){
        Accounts.forgotPassword({email: t.find('#email').value},
            function(err){
                if(err){
                    Template.popup.showPopupMessage(err);
                }else{
                    Router.go('/');
                }
            });
    }
};