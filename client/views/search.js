var tSession;
var beforeDate, afterDate;
var now = new Date();
var defaultCount = 5;

Template.search.created = function(){
    if(tSession == undefined){
        tSession = getTemplateSession();
    }
};

var includeDay = function(date) {
    date.setHours(23);
    date.setMinutes(59);
    date.setSeconds(59);
    date.setMilliseconds(999);
};

Template.search.rendered = function(){
    $(document).ready(function(){
        afterDate = $('#afterDate').datepicker().on('changeDate', function(ev) {
            if (ev.date.valueOf() > beforeDate.date.valueOf()) {
                var newDate = new Date(ev.date);
                beforeDate.setValue(newDate);
            }else{
                beforeDate.setValue(beforeDate.date); //hacker to trigger rendering/disable
            }
            afterDate.setValue(ev.date);
            afterDate.hide();
        }).data('datepicker');
        beforeDate = $('#beforeDate').datepicker({
            onRender: function(date) {
                return date.valueOf() < afterDate.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
                beforeDate.hide();
            }).data('datepicker');
    });

};

Template.search.destroyed = function(){
    $(window).off('resize');
};

Template.search.events({
    'click .selectSearch' : function(evt, tmp) {
        evt.preventDefault();
        tmp.find("#clickSearch").innerHTML = evt.target.innerHTML;
        var val = parseInt(evt.target.id);
        switch (val) {
            case 1:
                setIsSearchEvents(true);
                setIsSearchBrands(false);
                break;
            case 2:
                setIsSearchBrands(false);
                setIsSearchEvents(false);
                break;
            case 3:
                setIsSearchBrands(true);
                setIsSearchEvents(false);
                break;
            default:
                //wtf
        }
        setSearchQuery(tmp.find("#searchQuery").value);
    },

    "keydown #searchQuery" : function(evt, tmp){
        if(evt.keyCode == 13){
            evt.preventDefault();
            clickSearch(tmp);
        }
    },

    'click #clickSearch' : function(evt, tmp) {
        clickSearch(tmp);
    },

    'click .category' : function(evt, tmp) {
        $('#'+evt.target.id).blur();
        setCategory(evt.target.id);
    },

    'click #loadMore' : function(evt, tmp) {
        incrementSearchCount();
    }
});

var clickSearch = function(tmp) {
    var longitude = "";
    var latitude = "";
    var distance = "";
    var after = Template.search.getAfterTime();
    var before = Template.search.getBeforeTime();
    var location = [];
    if (Template.search.isSearchEvents() ||Template.search.isSearchBrands() ) {
        longitude = tmp.find("#longitude").value;
        latitude = tmp.find("#latitude").value;
        distance = tmp.find("#distance").value;
        if (Template.search.isSearchEvents()) {
            after = afterDate.date;
            includeDay(beforeDate.date);
            before = beforeDate.date;
        }
    }
    if (longitude.length > 0 && latitude.length > 0) {
        longitude = Number(longitude);
        latitude = Number(latitude);
        if (!isNaN(longitude) && !isNaN(latitude))
            location = [longitude, latitude];
    }
    if (distance.length > 0) {
        distance = Number(distance);
        if (isNaN(distance))
            distance = 10;
    }
    else {
        distance = 10;
    }

    resetSearchCount();
    setSearchOptions({
        categories: getCategory(),
        afterTime: after,
        beforeTime: before,
        location: location,
        distance: distance
    });
    setSearchQuery(tmp.find("#searchQuery").value);
};

Template.search.showAdvancedQuerying = function(){
    return true;
};

loadSearchSession = function() {
    if(tSession == undefined){
        tSession = getTemplateSession();
    }
    tSession.set("isSearchEvents", true);
    tSession.set("isSearchBrands", true); //only checked if isSearchEvents is false
    tSession.set("searchQuery", "");
    tSession.set("count", defaultCount);
    defaultSession(getLocation());
};

var defaultSession = function(coordinates) {
    var currDate = new Date();
    tSession.set("searchOptions",{
        categories: [],
        afterTime: currDate,
        beforeTime: currDate,
        location: coordinates,
        distance: 10
    });
};

unloadSearchSession = function() {
    tSession.set("isSearchEvents",undefined);
    tSession.set("isSearchBrands",undefined);
    tSession.set("searchOptions", undefined);
    tSession.set("searchQuery", undefined);
    tSession.set("count", undefined);
};

Template.search.categoryBtn = function(category) {
    return _.contains(tSession.get("searchOptions").categories, category) ? "btn-alizarin-nh" : "btn-default";
};

setCategory = function(category) {
    var options = tSession.get("searchOptions");
    var categories = _.extend([], options.categories);
    if (_.contains(options.categories, category)) {
        categories = _.without(options.categories, category);
    }
    else {
        categories.push(category);
    }
    options.categories = categories;
    tSession.set("searchOptions", options);
};

getCategory = function() {
    return tSession.get("searchOptions").categories;
};

var resetSearchCount = function() {
    tSession.set("count", defaultCount);
};

var incrementSearchCount = function() {
    tSession.set("count", getSearchCount() + defaultCount);
};

getSearchCount = function() {
    return tSession.get("count");
};

setSearchOptions = function(options) {
    tSession.set("searchOptions", options);
};

Template.search.getLongitude = function() {
    return tSession.get("searchOptions").location[0];
};

Template.search.getLatitude = function() {
    return tSession.get("searchOptions").location[1];
};

Template.search.getDistance = function() {
    return tSession.get("searchOptions").distance;
};

Template.search.getAfterTime = function() {
    return tSession.get("searchOptions").afterTime;
};

Template.search.getBeforeTime = function() {
    return tSession.get("searchOptions").beforeTime;
};

Template.search.getSearchTypeString = function() {
    if (Template.search.isSearchEvents())
        return strings["events"];
    else if (Template.search.isSearchBrands())
        return strings["brands"];
    else
        return strings["people"];
};

setIsSearchEvents = function(bool) {
    tSession.set("isSearchEvents", bool);
};

Template.search.isSearchEvents = function() {
    return tSession.get("isSearchEvents");
};

Template.search.getSearchEventOptions = function() {
    return tSession.get("searchOptions");
};

Template.search.isSearchBrands = function() {
    return tSession.get("isSearchBrands");
};

setIsSearchBrands = function(isBrand) {
    tSession.set("isSearchBrands", isBrand);
};

Template.search.getSearchBrandOptions = function() {
    var options = {};
    var searchOptions = tSession.get("searchOptions");
    options.location = searchOptions.location;
    options.distance = searchOptions.distance;
    options.categories = searchOptions.categories;
    return options;
};

setSearchQuery = function(query) {
    tSession.set("searchQuery", query);
};

Template.search.getSearchQuery = function() {
    return tSession.get("searchQuery");
};

getDateString = function(date){
    return (date.getMonth()+1).toString()+'/'+date.getDate().toString()+'/'+date.getFullYear().toString();
};

Template.search.getToday = function(){
    return getDateString(now);
};

Template.search.showDistance = function() {
    return getLocation().length === 0 ? "display:none" : "";
};