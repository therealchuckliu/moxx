/*
Routes file specifies what urls go to which templates, any restrictions
that certain pages have with respect to being logged in
 */

/*
Map url paths to specific templates
 */
Router.map(function () {
    this.route('admin', {
        path: '/admin',
        controller: 'AdminController'
    });

    this.route('forgotPassword',{
        path:'/forgotpassword'
    });

    this.route('register', {
        path: '/register'
    });

    this.route('requests',{
        path: '/requests',
        controller: 'RequestsController'
    });

    this.route('resetPassword',{
        path: '/resetpassword/:_id',
        controller: 'ResetPasswordController'
    });

    this.route('search', {
        path: '/search',
        controller: 'SearchController'
    });

    this.route('moxxboxx',{
        path: '/moxxboxx',
        controller: 'MoxxBoxxController'
    });

    this.route('chat',{
        path: '/moxxboxx/:_id',
        controller: 'ChatController'
    });

    this.route('notifications',{
        path: '/notifications',
        controller: 'NotificationController'
    });

    this.route('profile',{
        path: '/profile/:_username',
        controller: 'ProfileController'
    });

    this.route('profile',{
        path: '/profile',
        controller: 'RedirectController'
    });

    this.route('eventcreate', {
        path: '/eventcreate',
        load: function(){
            runDeps('eventcreate');
            setActiveNavView('eventcreate');
        }
    });

    this.route('account', {
        load: function(){
            runDeps('account');
            setActiveNavView('account');
        },
        path: '/account'
    });

    this.route('eventSuggestions',{
        path: '/suggestions/:_id',
        controller: 'EventSuggestionsController'
    });

    this.route('home',{
        path: '/',
        controller: 'HomeController'
    });

    this.route('connections',{
        path: '/following/:_username',
        controller: 'FollowingController'
    });

    this.route('connections',{
        path: '/followers/:_username',
        controller: 'FollowerController'
    });

    this.route('connections',{
        path: '/followers',
        controller: 'RedirectController'
    });

    this.route('connections',{
        path: '/following',
        controller: 'RedirectController'
    });

    this.route('attendees',{
        path: '/attendees/:_id',
        controller: 'AttendeesController'
    });

    this.route('eventedit',{
        path: '/edit/:_id',
        controller: 'EditEventController'
    });

    this.route('event',{
        path: '/event/:_id',
        controller: 'EventPageController'
    });

    this.route('notFound', {path: '*'});

});

/*
Configure general layout
 */
Router.configure({
    layoutTemplate: 'layout',
    notFoundTemplate: 'notFound',
    loadingTemplate: 'loading',

    before: function() {
        var routeName = this.route.name;
        // no need to check at these URLs
        if (_.include(['main','forgotPassword', 'resetPassword', 'register'], routeName))
            return;

        if (!Meteor.user()) {
            this.render(Meteor.loggingIn() ? this.loadingTemplate : 'main');
            return this.stop();
        }
        else {
            if (!Meteor.user().username) {
                //if no username in profile, means complete setup hasn't been done
                this.render('initialsetup');
                return this.stop();
            }
        }
    }
});


HomeController = RouteController.extend({
    template: 'home',

    load: function() {
        resetActiveNavView();
        loadHome();
        runDeps('home');
    },

    /*waitOn: function() {
        return Meteor.user() ?
            [
                Meteor.subscribe('myEvents', {count: getHomeCount()}),
                Meteor.subscribe('openEvents', getLocation(), {count: getHomeCount()})
            ] :
            []
    },*/

    data: function () {
            var arr = getAllEventsForHome();
            return  {
                eventData: arr.length > 0 ? arr : false,
                dependencies: ["myEvents", "openEvents"]
            };
    },

    unload: function() {
        unloadHome();
    }
});

RequestsController = RouteController.extend({
    template: 'requests',

    waitOn: function(){
        return Meteor.user() ? Meteor.subscribe('myFollowRequests') : [];
    },
    data: function(){
        return {requests: getMyFollowRequests()};
    }
});

NotificationController = RouteController.extend({
    template: 'notifications',

    load: function() {
        setActiveNavView('notifications');
        runDeps('notifications');
    },

    waitOn: function() {
        return Meteor.user() ? Meteor.subscribe("myNotifications", getNotificationCount()) : [];
    },

    data: function() {
        return {
            dependencies: ["myNotifications"]
        };
    },

    unload: function() {
        resetNotifications();
    }
});

ResetPasswordController = RouteController.extend({
    template: 'resetPassword',
    data : function(){
        return {
            token : this.params._id
        }
    }

});

ProfileController = RouteController.extend({
   template: 'profile',

    load: function() {
        loadProf(this.params._username);
        setActiveNavView('profile');
        runDeps('profile');
    },

    waitOn: function(){
        //subscribe to all of our own profile info, as well as the one of the given username (doesn't matter if the username is ours)
        return Meteor.user() ?
            [
                /*//find events in common with this person
                Meteor.subscribe('myEvents', {count: getProfCount(), username: getProfUsername()}),
                Meteor.subscribe('openEvents', getLocation(), {count: getProfCount(), username: getProfUsername()}),
                */
                //user info, if brand or you're following the person then all of profile published
                Meteor.subscribe("getUserByUsername", this.params._username)
            ]
        : [];
    },

    data: function(){
        var prof = Meteor.users.findOne({username: {$regex: new RegExp("^" + this.params._username.toLowerCase() + "$", "i")}});
        if (prof) {
            var numFollowing = prof.consumerFollowing ? prof.consumerFollowing.length : 0;
            numFollowing +=  prof.brandFollowing ? prof.brandFollowing.length : 0;
            var numFollowers = prof.consumerFollowers ? prof.consumerFollowers.length : 0;
            numFollowers +=  prof.brandFollowers ? prof.brandFollowers.length : 0;
            var myProfile = prof._id === Meteor.userId();
            setActiveNavView(myProfile ? 'profile' : undefined);
            var events = getMyEvents(prof._id);
            return {
                userInfo: prof,
                eventData : events.length > 0 ? events : false,
                numFollowing: numFollowing,
                numFollowers: numFollowers,
                myProfile : myProfile,
                dependencies: ["getUserByUsername", "myEvents", "openEvents"]
            };
        }
    },

    unload: function() {
        unloadProf();
    }
});

FollowingController = RouteController.extend({
    template : 'connections',

    load: function() {
        resetActiveNavView();
        runDeps('connections');
    },

    waitOn: function(){
        return Meteor.user() ? [
            Meteor.subscribe("getUserFollowing", this.params._username, true),
            Meteor.subscribe('getUserFollowing', this.params._username, false),
            Meteor.subscribe("getUserByUsername", this.params._username)
        ] : [];
    },

    data : function(){
        return{
            consumerData : getUserFollowing(this.params._username, false),
            brandData : getUserFollowing(this.params._username, true)
        }
    }
});

FollowerController = RouteController.extend({
    template : 'connections',

    load: function() {
        resetActiveNavView();
        runDeps('connections');
    },

    waitOn : function(){
        return Meteor.user() ?
        [
            Meteor.subscribe("getUserFollowers", this.params._username, true),
            Meteor.subscribe('getUserFollowers', this.params._username, false),
            Meteor.subscribe("getUserByUsername", this.params._username)
        ] : [];
    },

    data : function(){
        return{
            consumerData : getUserFollowers(this.params._username, false),
            brandData : getUserFollowers(this.params._username, true)
        }
    }
});

AttendeesController = RouteController.extend({
    template: 'attendees',

    load: function() {
        resetActiveNavView();
        runDeps('attendees');
    },

    waitOn: function(){
        return Meteor.user() ? Meteor.subscribe("getEventInfo", this.params._id, 0) : [];
    },

    data: function(){
        var event = getEventDataById(this.params._id);
        var network = undefined;//myNetwork();
        if (event)
            return{
                eventData: event,
                attendees: getEventAttendees(event, network),
                maybeAttending: getEventMaybeAttending(event, network),
                invited: getEventInvited(event, network),
                notAttending: getEventNotAttending(event, network)
            }
    }
});

EditEventController = RouteController.extend({
   template: 'eventedit',
    waitOn: function(){
        return Meteor.user() ? Meteor.subscribe('getEventInfo', this.params._id, 0) : [];
    },
    data: function(){
        return {
            eventData : getEventDataById(this.params._id)
        };
    }
});


EventSuggestionsController = RouteController.extend({
    template: 'eventSuggestions',

    load: function() {
        resetActiveNavView();
        loadEvent(this.params._id);
    },

    waitOn: function(){
        return Meteor.user() ? Meteor.subscribe("getEventInfo", this.params._id, 0) : []
    },

    data : function(){
        return{
            userData: getEventSuggestedUsers(this.params._id)
        }
    },

    unload: function() {
        unloadEvent();
    }
});

EventPageController = RouteController.extend({
    template : 'event',

    load: function() {
        resetActiveNavView();
        loadEvent(this.params._id);
        runDeps('event');
    },

    data : function(){
        var eventData = getEventDataById(this.params._id);
        var lat, lng;
        var ownerData = false;
        if (eventData) {
            if(eventData.location){
                lat = eventData.location.coordinates[1];
                lng = eventData.location.coordinates[0];
                setLatLong(lat,lng);
            }
            ownerData = Meteor.users.findOne({_id : eventData.owner});
            if (eventData.comments)
                //comments have loaded, this means we have all the data we need
                return{
                    eventData : eventData,
                    ownerData : ownerData,
                    myEvent : eventData.owner === Meteor.userId(),
                    dependencies: ["getEventInfo"]
                };
            else {
                return false;
            }
        }
    },

    unload: function() {
        unloadEvent();
    }
});

MoxxBoxxController = RouteController.extend({
    template : 'moxxboxx',

    load: function() {
        loadMoxxBoxx();
        setActiveNavView('moxxboxx');
        runDeps('moxxboxx');
    },

    waitOn : function(){
        return Meteor.user() ? Meteor.subscribe("myMoxxBoxx") : [];
    },

    data : function(){
        var myChats = myMoxxBoxx();
        var usernameHash = getMoxxHash();
        var gotAllInfo = true;
        myChats.forEach(function(chat) {
            var chatHash;
            if (usernameHash.hasOwnProperty(chat._id))
                chatHash = usernameHash[chat._id];
            else
                chatHash = {};
            gotAllInfo = gotAllInfo && updateUsernameHash(chat, chatHash);
            usernameHash[chat._id] = chatHash;
        });
        if (gotAllInfo) {
            setMoxxHash(usernameHash);
            return {
                members: usernameHash,
                myChats: myChats.count() > 0 ? myChats : false
            };
        }
        else {
            //still waiting on some data to be received
            this.subscribe("myMoxxBoxx");
            return false;
        }
    },

    unload: function() {
        unloadMoxxBoxx();
    }
});

ChatController = RouteController.extend({
    template : 'chat',

    load: function() {
        loadChatSession(this.params._id);
        resetActiveNavView();
        runDeps('chat');
    },

    data : function(){
        var chat = MoxxBoxx.findOne({_id: this.params._id, members: Meteor.userId()});
        if (chat) {
            var usernameHash = getUsernameHash();
            if (updateUsernameHash(chat, usernameHash)) {
                setUsernameHash(usernameHash);
                return{
                    _id: chat._id,
                    title : chat.title,
                    members : usernameHash,
                    messages: chat.messages,
                    dependencies: ["getChat"]
                };
            }
            else {
                //still waiting on data to be subscribed
                this.subscribe("getChat", this.params._id, Session.get("chatMessages"));
                return false;
            }
        }
    },

    unload: function() {
        unloadChatSession();
    }
});

SearchController = RouteController.extend({
    template : 'search',

    load: function() {
        loadSearchSession();
        setActiveNavView('search');
        runDeps('search');
    },

    data : function(){
        return {
            results: Template.search.isSearchEvents() ?
                searchEvents(Template.search.getSearchQuery(), Template.search.getSearchEventOptions())
                :
                searchUsers(Template.search.getSearchQuery(), Template.search.isSearchBrands(), Template.search.getSearchBrandOptions()),
            dependencies: Template.search.isSearchEvents() ? ["searchEvents"] : ["searchUsers"]
        };
    },

    unload: function() {
        unloadSearchSession();
    }
});

AdminController = RouteController.extend({
    template : 'admin',

    load : function(){
        setActiveNavView('admin');
        runDeps('admin');
    },
    waitOn : function(){
        return Meteor.user() ? Meteor.subscribe('admin') : [];
    },

    data : function(){
        return {
            eventData: Events.find(),
            userData: Meteor.users.find({'profile.hasBrand': false}),
            brandData: Meteor.users.find({'profile.hasBrand': true})
        };
    }
});

RedirectController = RouteController.extend({
    run: function() {
        var routeName = this.path.replace(/\//g, "");
        if (Meteor.user() && Meteor.user().username)
            this.redirect('/' + routeName + '/' + Meteor.user().username);
        else if(Meteor.loggingIn())
            this.render();
        else
            this.redirect('/');
    }
});
