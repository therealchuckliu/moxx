/*
 Retrieve specified sets of data related to events
 When in a template you want to get the search results, or friend results, use these functions
 */


//retrieves events that match a search query
searchEvents = function(query, options) {
    return sortEvents(Events.find(searchEventsQuery(query, options, Meteor.userId())));
};

//retrieve owners that own events you're invited to
getEventOwners = function(isBrand) {
    var ids =  getUserIds(Events.find({invited: Meteor.userId()}, {owner: 1, _id:0}).fetch(), "owner");
    return Meteor.users.find({$and: [{hasBrand: isBrand}, {_id: {$in: ids}}]});
};

getMyEvents = function(id){
    if (Meteor.user()) {
        return sortEvents(Events.find(myEvents(id)));
    }
    return false;
};

getAllEventsForHome = function(){
    return sortEvents(Events.find({$or: [{open: true}, {invited: Meteor.userId()}, {owner: Meteor.userId()}]}));
};

getEventDataById = function(id){
    return Events.findOne({$and: [{_id : id}, {$or: [{owner: Meteor.userId()}, {open: true}, {invited: Meteor.userId()}]}]});
};

getEventNameFromId = function(id) {
    var event= Events.findOne({_id:id});
    return event ? event.title : "NaN";
};

getEventAttendees = function(event, network){
    //var ids = event.owner === Meteor.userId() ? event.attending : _.intersection(event.attending, network);
    return getUserProfiles(event.attending);
};

getEventMaybeAttending = function(event, network){
    //var ids = event.owner === Meteor.userId() ? event.maybe : _.intersection(event.maybe, network);
    return getUserProfiles(event.maybe);
};

getEventInvited = function(event, network){
    //var ids = event.owner === Meteor.userId() ? event.invited : _.intersection(event.invited, network);
    return getUserProfiles(event.invited);
};

getEventSuggestedUsers = function(id){
    var ids = Events.findOne({_id : id}).suggested;
    return ids ? getUserProfiles(ids) : false;
};

getEventNotAttending = function(event, network){
    //var ids = event.owner === Meteor.userId() ? event.notAttending :  _.intersection(event.notAttending, network);
    return getUserProfiles(event.notAttending);
};

getUserProfiles = function(ids){
    return Meteor.users.find({_id : {$in : ids}});
};