/*
 Retrieve specified sets of data related to users
 When in a template you want to get the search results, or friend results, use these functions
 */

//retrieve the documents of users who this person is following
getUserFollowing = function(username, getBrand) {
    var query = userConnections(username, getBrand, "Following");
    return query ? Meteor.users.find(query) : false;
};

//retrieve the documents of users who is following this person
getUserFollowers = function(username, getBrand) {
    var query = userConnections(username, getBrand, "Followers");
    return query ? Meteor.users.find(query) : false;
};

//retrieves the users that match a search query
searchUsers = function(query, isBrand, options) {
    if (query.length > 0 || isBrand) {
        return Meteor.users.find(searchUserQuery(query, isBrand, options));
    }
    return false;
};

//retrieve specific user info
getUser = function(username) {
    return Meteor.users.findOne({username: {$regex: new RegExp("^" + username.toLowerCase() + "$", "i")}});
};

getUserImageFromId = function(id){
    var prof = Meteor.users.findOne({_id: id});
    return prof.profile.uploadedpic[0].src;
};

getUsernameFromId = function(id) {
    var prof = Meteor.users.findOne({_id: id});
    return prof ? prof.username : "NaN";
};

getMyFollowRequests = function(){
    if(Meteor.user()){
        var consumerReq = Meteor.user().consumerReqReceived ? Meteor.user().consumerReqReceived : [];
        var brandReq = Meteor.user().brandReqReceived ? Meteor.user().brandReqReceived : [];
        var arr = _.union(consumerReq,brandReq);
        return Meteor.users.find({_id: {$in : arr}});
    }
    return [];
};