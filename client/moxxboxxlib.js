/*
 Retrieve specified sets of data related to your moxxboxx
 */

//updates the usernameHash of id -> username given a chat
updateUsernameHash = function(chat, usernameHash) {
    var removedKeys = _.difference(_.keys(usernameHash), chat.members);
    for (var i = 0 ; i < removedKeys.length ; i++) {
        delete usernameHash[removedKeys[i]];
    }
    for (var i = 0 ; i < chat.members.length ; i++) {
        if (!usernameHash.hasOwnProperty(chat.members[i])) {
            var prof = Meteor.users.findOne({_id: chat.members[i]});
            if (prof) {
                usernameHash[chat.members[i]] = prof.username;
            }
            else {
                return false;
            }
        }
    }
    return true;
};

myMoxxBoxx = function() {
    return MoxxBoxx.find({members: Meteor.userId()});
};

getChatNameFromId = function(id) {
    var chat = MoxxBoxx.findOne({_id:id});
    return chat ? chat.title : "NaN";
};