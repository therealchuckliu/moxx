/*
    subscribe calls are moved to routes.js since what's returned are sometimes required
 */

//Obtain your initial location
var initLocation = function() {
    Session.setDefault("myLocation", []);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            Session.set("myLocation", [position.coords.longitude, position.coords.latitude]);
        });
    }
};

getLocation = function() {
    return Session.get("myLocation");
};

//Startup
Meteor.startup(function () {
    filepicker.setKey("A1YpuKcOuRL2nAM6sGPjHz");
    filepicker.constructWidget(document.getElementById('attachment'));
    Session.setDefault("popupMessage", "");
    Session.setDefault("popupTitle", "");
    Session.setDefault("showMyModal",false);
    Session.setDefault("notificationCount", defaultNotificationCount);
    initLocation();
});

Handlebars.registerHelper('myHasAll', function(){
    if (this.dependencies instanceof Array) {
        var hasAll = true;
        for (var i = 0 ; i < this.dependencies.length ; i++) {
            var hasDep = HasAll.findOne(this.dependencies[i]);
            if (hasDep)
                hasAll = hasAll && hasDep.hasAll;
        }
        return hasAll;
    }
    return true;
});

Handlebars.registerHelper('getString',function(index){
    return strings[index];
});

Handlebars.registerHelper('showSession', function(name) {
    return new Handlebars.SafeString(Session.get(name));
});

Handlebars.registerHelper('safeString', function(string) {
    return new Handlebars.SafeString(string);
});

Handlebars.registerHelper('reverse', function(arr){
    return arr.reverse();
});

Handlebars.registerHelper('printCurrentContext', function(obj){
    return (JSON.stringify(obj, null, 4));
});

Handlebars.registerHelper('arrayify',function(obj){
    result = [];
    for (var key in obj) {
        result.push({name: key, value: obj[key]});
    }
    return result;
});

Handlebars.registerHelper('getDoubleContext', function(ctx1, ctx2){
    var context = {one: ctx1, two: ctx2};
    return context;
});

Handlebars.registerHelper('hours',function(){
    return hours;
});

Handlebars.registerHelper('minutes', function(){
    return minutes;
});

Handlebars.registerHelper('ampm',function(){
    return ampm;
});

Handlebars.registerHelper('getRangedInclusiveArray', function(start, end){
    result = [];
    var current = start;
    while(current <= end){
        result.push({value: current++});
    }
    return result;
});

Handlebars.registerHelper('formattedTime', function(date){
    var dateObj = new Date(date);
    var hour = dateObj.getHours();
    var minutes = '0'+dateObj.getMinutes();
    var amorpm = parseInt(hour) > 11 ? ampm['pm'] : ampm['am'];
    hour = parseInt(hour) > 12 ? hour - 12 : hour;
    return hour.toString()+':'+minutes.substring(minutes.length-2,minutes.length)+amorpm;
});

Handlebars.registerHelper('formattedDay', function(date){
    var dateObj = new Date(date);
    return days[dateObj.getDay()];
});

Handlebars.registerHelper('loginOrLogoutId', function(){
    if(Meteor.user()){
        return 'logout-button';
    }else{
        return 'login-button';
    }

});

Handlebars.registerHelper('getLoginOrLogoutText', function(){
    if(Meteor.user()){
        return strings['logout'];
    }else{
        return strings['login'];
    }
});

Handlebars.registerHelper('formattedDate', function(date){
    var dateObj = new Date(date);
    return dateObj.toLocaleDateString();
});

Handlebars.registerHelper('verifyText', function(name, tSession){
    return tSession.get(name) ? "Verify" : "Verified";
});

/*
    Suppose you have an array of objects with consistent fields, i.e.
    array = [{name:"Charles", gender: "Female"}, {name:"Cameron", gender:"Male"}]
    getObjFromIndex(array, "name", 1) returns "Cameron"
    getObjFromIndex(array, "gender", 0) returns "Male"
    Leaving out the index automatically returns the last element (for most recent profile pics)
    in html write {{getObjFromIndex noquotesnameoffield "quotesnameofitem" 3}}
 */
Handlebars.registerHelper('getObjFromIndex', function(array, item, index){
    if (typeof index != "number")
        index = array.length-1;
    return array[index][item];
});

/*
    Returns the element of a given object.
    Example: obj = {name: "Charles", age: 23}
    getObj(obj, "name") return "Charles"
 */
Handlebars.registerHelper('getObj', function(obj, item){
    return obj[item];
});

Handlebars.registerHelper('e_categories', function(){
    return e_categories;
});

Handlebars.registerHelper('rsvp_options',function(){
    return rsvp_code_strings_invited;
});

Handlebars.registerHelper('days', function(){
    return days;
});

Handlebars.registerHelper('getCategoryString', function(name){
    return e_categories[name];
});

Handlebars.registerHelper('headerText', function(){
    return Meteor.user().profile.hasBrand ? strings['businessCategory'] : strings['interests'];
});

Handlebars.registerHelper('getUsernameFromId', function(userId){
    return getUsernameFromId(userId);
});

Handlebars.registerHelper('timeStampStr', function(timestamp){
    return timeStampString(timestamp);
});

Handlebars.registerHelper('getValidBorderClass', function(isValid){
    return isValid ? 'valid-border' : 'invalid-border';
});

Handlebars.registerHelper('getTimePassedSince', function(timestamp){
   return getTimeDifferenceString(timestamp);
});

Handlebars.registerHelper('length', function(arr){
    return arr.length;
});

Handlebars.registerHelper('equals', function(val1, val2){
    return val1 === val2;
});

Handlebars.registerHelper('newNotificationsString', function(){
    var count = Meteor.user() ? Meteor.user().newNotifCount : 0;
    return count > 0 ?  new Handlebars.SafeString('<span class="notifications">' + count + '</span>') : "";
});

Handlebars.registerHelper('or', function(val1, val2){
    return val1 || val2;
});

Handlebars.registerHelper('and', function(val1,val2){
    return val1 && val2;
});

Handlebars.registerHelper('recString', function(recString){
    var ret = "";
    var count = 0;
    for (var i in days) {
        if (recString.charAt(count++) == 1) {
            if (ret != "")
                ret += ", ";
            ret += days[i];
        }
    }
    return "Every "+ret;
});

Handlebars.registerHelper('nonEmptyString', function(stringVal){
    return stringVal.length > 0;
});

Handlebars.registerHelper('HHMM', function(time){
    var hours = (Math.floor(((time/100)) % 12));
    hours = hours == 0 ? "12" : hours.toString();
    hours = hours.length ==1 ? "0" + hours : hours;
    var minutes = (time % 100).toString();
    minutes = minutes.length == 1 ? "0" + minutes : minutes;
    return hours + ":" + minutes +  (time >= 1200 ? "PM" : "AM")
});

Handlebars.registerHelper('adminEmail', function(time){
    return Meteor.user() ? adminEmail(Meteor.user().emails[0].address) : false;
});

constantsStringSelector = function(name, tSession, trueKey, falseKey){
    return tSession.get(name) ? strings[trueKey] : strings[falseKey];
};

formatChatMembers = function(usernameHash) {
    var htmlString = '';
    var usernameKeys = _.keys(usernameHash);
    for (var i = 0, j = Math.min(usernameKeys.length, 3) ; i < j ; i++) {
        htmlString = htmlString +
            '<a class href="/profile/' + usernameHash[usernameKeys[i]] + '">' +
            usernameHash[usernameKeys[i]] + '</a>';
        if (i != j-1)
            htmlString = htmlString + ', ';
    }
    if (usernameKeys.length > 3)
        htmlString = htmlString + " and " + (usernameKeys.length-3) + (usernameKeys.length-3 == 1 ? " other" : " others");
    return new Handlebars.SafeString(htmlString);
};

triButtonModeSelector = function(name, tSession, nonVerfiedString, verifiedString, defaultString){
    if(!tSession.get(name)){
        return defaultString;
    }else if(tSession.get((name+'BtnMode'))){
        return verifiedString;
    }else{
        return nonVerfiedString;
    }
};

isEnabled = function(name, tSession){
    return stringSelector(name, tSession, '', 'disabled');
};

stringSelector = function(name, tSession, trueString, falseString){
    return tSession.get(name) ? trueString : falseString;
};

tryLogin = function(user, pass) {
    Meteor.call('correctCase', {userInfo: user}, function(error, result) {
        if(error){
            //Template.popup.showPopupMessage("Error", error.reason || "Unknown error occurred");
        }else{
            Meteor.loginWithPassword(
                result,
                pass,
                function(err){
                    if (err) {
                        // handle error
                        //Template.popup.showPopupMessage("Login Error", "Incorrect login details. Please try again.");
                    } else {
                        //success
                        Router.go('/');
                    }
                });
        }
    });
};

checkUsernameAvailability = function(text){
    if (text.replace(/[a-z0-9]+$/ig, "") != "" || text.length == 0) {
        return false;
    }else{
        Meteor.call('validUsername', {username: text}, function(error, result) {
            if(error){
                return false;
            }else{
                return result;
            }
        });
    }
};

checkAvailability = function(text, tSession, key){
    if (text.replace(/[a-z0-9]+$/ig, "") != "" || text.length == 0) {
        tSession.set(key,false)
    }else{
        Meteor.call('validUsername', {username: text}, function(error, result) {
            if(error){
                Template.popup.showPopupMessage("Error", error.reason || "Unknown error occurred");
            }else{
                tSession.set(key,result);
            }
        });
    }
};

geocodeAddress = function(addressVal, callback){
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': addressVal}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            callback({lat:results[0].geometry.location.lat(),lng: results[0].geometry.location.lng()});
        } else {
            callback({});
        }
    });
};

tryLogout = function() {
    if (Meteor.user()) {
        Meteor.logout(function(err){
            if (err) {
                // handle error
                Template.popup.showPopupMessage("Logout Error", "Unable to log out. Please try again.")
            } else {
                Router.go('/');
            }
        });
    }
};


getTemplateSession = function(){
    return {
        keys: {},
        dependencies: {},

        get: function(key){
            if(!this.dependencies[key]){
                this.dependencies[key] = new Deps.Dependency;
            }
            this.dependencies[key].depend();
            return this.keys[key];
        },

        set: function(key, value){
            this.keys[key] = value;
            if(this.dependencies[key]){
                this.dependencies[key].changed();
            }
        },

        isDefined: function(key){
            if(!this.dependencies[key]){
                this.dependencies[key] = new Deps.Dependency;
            }
            this.dependencies[key].depend();
            return this.keys[key] != undefined;
        }
    }
};

setConvertedImage = function(inkblob, tSession) {
    tSession.set("imagesource","f");
    filepicker.convert(inkblob, {width: 500, height: 500},
        function(new_InkBlob){
            tSession.set("imagesource",new_InkBlob.url);
        },
        function(error) {
            tSession.set("imagesource",inkblob.url);
        }
    );
};

setActiveNavView = function(view){
    Session.set("active",view);
};

resetActiveNavView = function(){
    Session.set('active',undefined);
};

Handlebars.registerHelper('isNavActive', function(navView){
    return Session.get("active") == navView ? 'active' : '';
});

getImageSourceFromTSession = function(tSession){
    if(!tSession.isDefined('imagesource')){
        tSession.set('imagesource','/img/default.jpg');
    }
    return tSession.get('imagesource');
};

timeStampString = function(timeStamp) {
    return dateString(new Date(timeStamp));
};

dateString = function(date) {
    return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
};

getSeconds = function(milliseconds){
    return Math.floor(milliseconds/1000);
};

getMinutes = function(milliseconds){
    return Math.floor(getSeconds(milliseconds)/60);
};

getHours = function(milliseconds){
    return Math.floor(getMinutes(milliseconds)/60);
};

getDays = function(milliseconds){
    return Math.floor(getHours(milliseconds)/24);
};

getMonths = function(milliseconds){
    return Math.floor(getDays(milliseconds)/28);
};

getYears = function(milliseconds){
    return Math.floor(getDays(milliseconds)/365);
};

getTimeDifferenceString = function(timestamp){
   var difference = (new Date()).valueOf() - timestamp.valueOf();
    if(getSeconds(difference) < 120){
        return time_passed['justnow'];
    }else if(getMinutes(difference) < 60){
        return getMinutes(difference) + ' ' + time_passed['minutesago'];
    }else if(getHours(difference) < 2){
        return time_passed['onehourago'];
    }else if(getHours(difference) < 24){
        return getHours(difference) + ' '+time_passed['hoursago'];
    }else if(getDays(difference) < 2){
        return time_passed['onedayago'];
    }else if(getDays(difference) < 30){
        return getDays(difference) + ' ' + time_passed['daysago'];
    }else{
        return time_passed['awhileago'];
    }
};

hasRequestedToFollowMe = function(userInfo){
    var reqReceived;
    if(userInfo.profile.hasBrand){
        reqReceived = Meteor.user().brandReqReceived;
    }else{
        reqReceived = Meteor.user().consumerReqReceived;
    }
    return _.contains(reqReceived, userInfo._id);
};

getFollowString = function(ctx, tSession){
    var isBrandProfile = ctx.profile.hasBrand;
    var followRequestSent = false;
    var following = false;
    if(isBrandProfile){
        if(_.contains(Meteor.user().brandReqSent, ctx._id)){
            followRequestSent = true;
        }else{
            following = _.contains(Meteor.user().brandFollowing, ctx._id);
        }
    }else{
        if(_.contains(Meteor.user().consumerReqSent, ctx._id)){
            followRequestSent = true;
        }else{
            following = _.contains(Meteor.user().consumerFollowing, ctx._id);
        }
    }
    if(tSession){
        tSession.set("following",following);
        tSession.set("followRequestSent", followRequestSent);
        tSession.set('isBrandProfile',isBrandProfile);
    }
    return followRequestSent ? strings['revokeFollowRequest'] :
        following ? strings['unfollow'] : isBrandProfile ? strings['follow'] : strings['sendFollowRequest'];
};

updateMyConnectionToUser = function(id, doIFollowUser, isBrandProfile, hasFollowRequestBeenSent){
    var _id = id.toString();
    var methodToCall = (doIFollowUser || isBrandProfile)  ? 'updateMyFollowing' : 'updateFollowingRequest';
    var addOrRemove = doIFollowUser ? false : isBrandProfile ? true : !hasFollowRequestBeenSent;

    Meteor.call("updateFollowingRequest", {id: _id, addRemove: addOrRemove}, function(err,res){
        if(err){
            alert(err);
        }else{
            alert(res);
        }
    });
};

approveFollowRequest = function(id){
  Meteor.call("updateMyFollowers", {id: id.toString(), addRemove : true});
};

ignoreFollowRequest = function(id){
    removeFollower(id);
};

removeFollower = function(id){
    Meteor.call('updateMyFollowers', {id: id.toString(), addRemove: false});
};

