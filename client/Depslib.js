/*
*   Keep all Deps.autorun functions in one place, stop and run the appropriate ones in routes.js
*
 */

/*
    Meteor only auto-publishes profile and username to Meteor.user()
    We store a lot of data outside of the profile document and this publish
    function will provide that information. It also splices the number of
    notifications
 */

/*this is a custom collection specifically to store whether all results on your page have been viewed, i.e. on a
search page, chat page, or your feed */
HasAll = new Meteor.Collection("hasAll");


Deps.autorun(function() {
    if (Meteor.userId() != undefined) {
        var count = getNotificationCount();
        if (count) {
            Meteor.subscribe("myNotificationsArray", count);
            //this is published in certain functions that update the HasAll collection
            Meteor.subscribe("hasAll");
        }
    }
});

/*
    These are other deps autoruns needed for certain routes, runDeps(template)
    is called below to initiate/stop them from the load point of every route
 */
DepsHash = {
    'home' : function() {
        var count = getHomeCount();
        if (count != undefined) {
            Meteor.subscribe('myEvents', {count: count});
            Meteor.subscribe('openEvents', getLocation(), {count: count});
        }
    },
    'search' : function() {
        var count = getSearchCount();
        var categories = getCategory();
        if (count != undefined && categories != undefined) {
            if (Template.search.isSearchEvents())
                return Meteor.subscribe("searchEvents", Template.search.getSearchQuery(), Template.search.getSearchEventOptions(), count, categories);
            else
                return Meteor.subscribe("searchUsers", Template.search.getSearchQuery(), Template.search.isSearchBrands(), Template.search.getSearchBrandOptions(), count, categories);
        }
    },
    'chat' : function() {
        var id = getChatId();
        var count = getChatCount();
        if (id && count != undefined) {
            Meteor.subscribe("getChat", id, count);
        }
    },

    'event' : function() {
        var id = getEventId();
        var count = getCommentCount();
        if (id && count != undefined)
            Meteor.subscribe("getEventInfo", id, count);
    },
    'profile' : function() {
        var count = getProfCount();
        var username = getProfUsername();
        if (username)
            username = username.toLowerCase();
        var prof = Meteor.users.findOne({username: {$regex: new RegExp("^" + username + "$", "i")}});
        if (count != undefined && prof) {
            //find events in common with this person
            Meteor.subscribe('myEvents', {count: count, userId: prof._id, myNetwork: inMyNetwork(prof._id, Meteor.userId())});
            Meteor.subscribe('openEvents', [], {count: count, userId: prof._id, myNetwork: inMyNetwork(prof._id, Meteor.userId())});
        }
    }
};

//the current computation object that stores the deps
var currentComputation;

//this should be called in the load function of any route that requires a dep
runDeps = function(tmp) {
    if (currentComputation && !currentComputation.stopped)
        currentComputation.stop();
    if (tmp != undefined && DepsHash.hasOwnProperty(tmp) && Meteor.userId() != undefined)
        currentComputation = Deps.autorun(DepsHash[tmp]);
    else
        currentComputation = undefined;
};