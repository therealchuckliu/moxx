/*
    Model.js

    -  Initialize Collections, define what's allowed/disallowed for the client to do, ie write/delete capabilities
    -  Define meteor methods, these are methods that client side calls because they need to communicate with server
       typically because they need to write some information to db that they don't have rights to
*/

////////////////////////
// Create Collections //
////////////////////////
/*
 **Events Model**
 */
Events = new Meteor.Collection("events");
Events.allow({

    // Cannot direct insert //
    insert: function (userId,event) {
        return false;
    },
    // *Can't update for now //
    update: function (userId, event, fields, modifier) {
        return false;
    },
    // *Can't remove yourself for now //
    remove: function (userId, event) {
        return false;
    }
});

/*
 **Moxxboxx Model**
 */
MoxxBoxx = new Meteor.Collection("moxxboxx");
MoxxBoxx.allow({

    // Cannot direct insert //
    insert: function (userId,chat) {
        return false;
    },
    // *Can't update for now //
    update: function (userId, chat, fields, modifier) {
        return _.contains(chat.members, userId) && !_.contains(fields,'members');
    },
    // *Can't remove yourself for now //
    remove: function (userId, chat) {
        return false;
    }
});

/*
 **Users Model (builtin)**
 */
//Users = new Meteor.Collection("users");
Meteor.users.allow({

    // Cannot direct insert //
    insert: function (userId,event) {
        return false;
    },
    // *Can't update for now //
    update: function (userId, event, fields, modifier) {
        return false;
    },
    // *Can't remove yourself for now //
    remove: function (userId, user) {
        return userId == user._id;
    }
});

/*
    Common code that will be used between server and client
    Includes code for publishing/retrieving data from client
 */

getUserIds = function(arr, field) {
    var IdsArr = [];
    for (var i = 0 ; i < arr.length ; i++) {
        IdsArr.push(arr[i][field]);
    }
    return IdsArr;
};

/*
    Check if something is a valid email
*/
validEmail = function (x) {
    var atpos=x.indexOf("@");
    var dotpos=x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
    {
        return false;
    }
    return true;
};

/*
    Converting a date object to HHMM
 */
dateToHHMM = function(d) {
    return d.getHours()*100 + d.getMinutes();
};


/*
    number of days between two date objects
 */
numDays = function(startDate, endDate) {
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((endDate.getTime() - startDate.getTime())/(millisecondsPerDay)));;
};

/*
    Creating the mongo query for a regex recurring String given date objects
    How it's created is by checking if there are 7 days recurring - if so just return generic regex date
    If there are some days excluded, create a regex string that sets all days to 0, then return
    a query that checks if it DOESNT match that regex
 */
dateToRegexRecString = function(d1, d2) {
    if (numDays(d1,d2)<7) {
        var day1 = d1.getDay();
        var day2 = d2.getDay();
        var day1day2 = genRecString(day2>=day1? day2 - day1 + 1 : 7 - day1);
        var afterday2 = day2>=day1 && day2 < 6 ? "\\d{" + (6 - day2) + "}" : "";
        var beforeday1 = day2>=day1 > 0 ? (day1 > 0 ? "\\d{" + day1 + "}" : "") : genRecString(day2+1) + "\\d{" + (day1-day2-1) + "}";
        beforeday1 = "^" + beforeday1;
        afterday2 = afterday2 + "$";
        return {$not: new RegExp(beforeday1 + day1day2 + afterday2)};
    }
    else return {$regex: new RegExp("^\\d{7}$")};
};

var genRecString = function(num) {
    return "[0]{" + num + "}";
};

locationQuery = function(location, distance) {
    return {$near: {$geometry: {type: "Point", coordinates: location}}, $maxDistance: distance*1609.34};
};

//check that you have a moxx.co email
adminEmail = function(email) {
    var domain = "@moxx.co";
    return email.indexOf(domain, email.length - domain.length) !== -1;
};

if (!String.formatName) {
    String.prototype.formatName = function() {
        return this.length > 1 ? this.substring(0,1).toLocaleUpperCase() + this.substring(1,this.length).toLocaleLowerCase() : this.toLocaleUpperCase();
    };
}

if(!String.linkify) {
    String.prototype.linkify = function() {

        // http://, https://, ftp://
        var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;

        // www. sans http:// or https://
        var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

        // Email addresses
        var emailAddressPattern = /\w+@[a-zA-Z_]+?(?:\.[a-zA-Z]{2,6})+/gim;

        return this
            .replace(urlPattern, '<a href="$&">$&</a>')
            .replace(pseudoUrlPattern, '$1<a href="http://$2">$2</a>')
            .replace(emailAddressPattern, '<a href="mailto:$&">$&</a>');
    };
}

convertMessage = function(message) {
    message = message.linkify().replace(/\n/g, '<br />');
    var regExp = /[^<]*[^ ]*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^<#\&\? ]*)[^>]*/;
    var match = message.match(regExp);
    if (match && match[2].replace(/\W/g, '').length == 11){
        var src = "http://www.youtube.com/embed/" + match[2].replace(/\W/g, '');
        message = message.replace(match[0], 'iframe width="640" height="360" src="' + src + '" frameborder="0" allowfullscreen></iframe><br /');
    }
    return message;
};