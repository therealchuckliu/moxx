/*
 Server functions for updating users, brands, events
 */

Meteor.methods({
    lastSeenNotification : function() {
        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");
        Meteor.users.update({_id: this.userId}, {$set: {newNotifCount: 0}});
    },

    clickNotification: function(options) {
        check(options, {notificationParams: Object});

        if (!this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        if (Meteor.isServer)
            Meteor.users.update({_id: this.userId, notifications: {$elemMatch: options.notificationParams}}, {$set: {"notifications.$.clicked" :true}});
    },

    deleteNotification : function(options) {
        check(options,{
            notificationParams: Match.Any
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var query = {};
        for (var key in options.notificationParams) {
            if (options.notificationParams.hasOwnProperty(key)) {
                query[key] = options.notificationParams[key];
            }
        }
        Meteor.users.update(
            {_id: this.userId},
            {
                $pull: {notifications: query}
            });
    }
});

/*
   *
   * Notifications Setup
   *
*/

notificationTypes = {
    following : 1,
    followingrequest : 2,
    eventInfo: 3,
    eventInvite: 4,
    eventRSVP: 5,
    eventLike: 6,
    eventComment: 7,
    eventCommentLike: 8,
    addChatMember: 9,
    addEventPollTime: 10,
    removeEventPollTime: 11,
    chooseEventPollTime: 12
};

/*
    these notifications don't require incrementing your
    new notifications count if there already exists one that it'd be grouped with
 */
var noIncrementNotifCount =
    [
        notificationTypes.eventInfo,
        notificationTypes.eventLike,
        notificationTypes.eventComment,
        notificationTypes.eventCommentLike,
        notificationTypes.chooseEventPollTime
    ];

/*
 Adds a notification to your profile document. Will group similar notifications
 together based on notification type, updating the timestamp.

 Any notification params should contain:
 notificationType : number as defined in upper notificationTypes object
 users : array of users involved
 link : link for where to go upon clicking the notification
 timeStamp: time that notification was sent

 If eventNotification:
 eventId : id of event
 */
addNotification = function(userIds, options) {
    var query = {};
    for (var key in options) {
        if (key != "users" && options.hasOwnProperty(key)) {
            query[key] = options[key];
        }
    }
    var matches = getUserIds(Meteor.users.find({$and:[
            {_id: {$in: userIds}},
            {notifications: {$elemMatch: query}}
    ]}, {_id:1}).fetch(), "_id");

    options.timeStamp = (new Date()).getTime();
    options.clicked = false;
    var newNotif = _.difference(userIds, matches);

    Meteor.users.update(
        {_id: {$in: newNotif}},
        {$addToSet: {notifications: options},
         $inc: {newNotifCount: 1}},
        {multi: true}
    );

    var incAmount = _.contains(noIncrementNotifCount, options.notificationType) ? 0 : 1;
    if (Meteor.isServer)
        Meteor.users.update(
            {$and:[
                {_id: {$in: matches}},
                {notifications: {$elemMatch: query}}
            ]},
            {
                $addToSet: {"notifications.$.users": options.users[0]},
                $set: {"notifications.$.clicked" : false,
                       "notifications.$.timeStamp": options.timeStamp},
                $inc: {newNotifCount: incAmount}
            },
            { multi: true}
        );
};