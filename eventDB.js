/*
 Server functions for fetching data about users, brands, events
 */

Meteor.methods({
    // Create Events //
    createEvent: function (options) {
        check(options, {
            title : String,
            tagline : String,
            description : String,
            uploadedpic: String,
            timeStart: Date,
            timeEnd: Date,
            address: String,
            location: [Number],   //[longitude,latitude]
            open: Boolean,
            categories: [String],
            recurring: Boolean,
            recString: String
        });
        if (!this.userId)
            throw new Meteor.Error(404, "You must be logged in to do this.");

        var doc = {};
        doc["active"] = true;
        doc["owner"] = this.userId;
        doc["title"] = options.title;
        doc["tagline"] = options.tagline;
        doc["description"] = options.description;
        doc["uploadedpic"] = options.uploadedpic;
        doc["address"] = options.address;
        if (options.location.length == 2)
            doc["location"] = {type: "Point", //setup for mongodb 2dsphere indexes
                            coordinates: options.location};
        doc["open"] = options.open;
        doc["invited"] = [];
        doc["suggested"] = [];
        doc["attending"] = [this.userId];
        doc["maybe"] = [];
        doc["notAttending"] = [];
        doc["categories"] = options.categories;
        doc["like"] = [];
        doc["comments"] = [];
        if (options.recurring) {
            if (options.recString.length != 7)
                throw new Meteor.Error(404, "You have selected an invalid list of days.");

            doc["HHMMStart"] = dateToHHMM(options.timeStart);
            if (options.timeEnd > options.timeStart)
                doc["HHMMEnd"] = dateToHHMM(options.timeEnd);
            doc["recurring"] = options.recString;
        }else {
            doc["timeStart"] = options.timeStart;
            if (options.timeEnd > options.timeStart)
                doc["timeEnd"] = options.timeEnd;
        }

        return Events.insert(doc);
    },

    eventActivation: function(options) {
        check(options, {
            eventId: String,
            isActive: Boolean
        });

        var event = Events.findOne({_id: options.eventId});
        if (!event || !this.userId || event.owner !== this.userId)
            throw new Meteor.Error(404, "No such event.");

        Events.update({_id: event._id}, {$set: {active: options.isActive}});
    },

    //update event info
    updateEventInfo : function(options){
        check(options,{
            eventId : String,
            field: String,
            value: Match.Any
        });

        var event = Events.findOne(options.eventId);
        if (! event || event.owner !== this.userId)
            throw new Meteor.Error(404, "No such event");

        var whatToUpdate = {};
        whatToUpdate["$set"] = {};
        whatToUpdate["$set"][options.field] = options.value;
        Events.update({_id: options.eventId}, whatToUpdate);
        addNotification(event.open ? event.attending : event.invited,
            {
                notificationType: notificationTypes.eventInfo,
                users: [event.owner],
                link: "/event/" + event._id,
                eventId: event._id
            });
    },

    updateEventCategory : function(options){
        check(options,{
            eventId: String,
            category : String,
            removeFromDB : Boolean
        });

        if (! this.userId || Events.findOne({_id: options.eventId}).owner !== this.userId)
            throw new Meteor.Error(403, "You must be the owner of this event to do this.");

        if(options.removeFromDB){
            Events.update({_id: options.eventId}, {$pull: {categories : options.category}});
        }else{
            Events.update({_id: options.eventId}, {$addToSet: {categories : options.category}});
        }
        return options.removeFromDB;
    },

    //invite someone to an event
    invite: function(options) {
        check(options, {
            eventId: String,
            usernames: [String]
        });
        var event = Events.findOne(options.eventId);
        if (! event || (event.owner !== this.userId && !event.open && !_.contains(event.invited, this.userId)))
            throw new Meteor.Error(404, "No such event");
        var userIds = [];
        for (var i = 0 ; i < options.usernames.length ; i++) {
            var profile = Meteor.users.findOne({username: {$regex: new RegExp("^" + options.usernames[i].toLowerCase() + "$", "i")}});
            if (profile && !_.contains(userIds, profile._id) && event.owner !== profile._id && this.userId !== profile._id) {
                userIds.push(profile._id);
            }
        }

        if (event.owner === this.userId || event.open) {
            Events.update({_id:options.eventId}, { $pullAll: {suggested: _.intersection(userIds, event.suggested) },
                $addToSet: { invited: {$each: userIds} } });
            addNotification(userIds,
                {
                    notificationType: notificationTypes.eventInvite,
                    users: [this.userId],
                    link: "/event/" + event._id,
                    eventId: event._id
                });
        }
        else
            Events.update({_id:options.eventId}, { $addToSet: { suggested: {$each: userIds} } });
    },

    removeSuggestion: function(options) {
        check(options, {
            eventId: String,
            userId: String
        });

        var event = Events.findOne(options.eventId);
        if (! event || event.owner !== this.userId)
            throw new Meteor.Error(404, "No such event");

        Events.update({_id:options.eventId}, { $pull: {suggested: options.userId }});
    },

    //update an rsvp for an event
    updateRSVP : function(options) {
        check(options,{
            eventId : String,
            rsvp : Number
        });
        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to RSVP");

        var event = Events.findOne(options.eventId);
        if (! event)
            throw new Meteor.Error(404, "No such event.");

        if (! event.open && event.owner !== this.userId &&
            !_.contains(event.invited, this.userId))
            throw new Meteor.Error(403, "No such event");

        var responseString = getResponseString(options.rsvp);
        //you are in the invite list, hence you may have selected notAttending or maybe so need to pull from there
        if (_.contains(event.invited, this.userId) ) {
            var modifier = {};
            modifier["$addToSet"] = {};
            modifier["$addToSet"][responseString] = this.userId;
            modifier["$pull"] = {};
            if (responseString != 'attending' && _.contains(event.attending, this.userId))
                modifier["$pull"]['attending'] = this.userId;
            if (responseString != 'notAttending'  && _.contains(event.notAttending, this.userId))
                modifier["$pull"]['notAttending'] = this.userId;
            if (responseString != 'maybe'  && _.contains(event.maybe, this.userId))
                modifier["$pull"]['maybe'] = this.userId;
            Events.update(options.eventId, modifier);
            if (responseString == 'attending')
                addNotification([event.owner],
                    {
                        notificationType: notificationTypes.eventRSVP,
                        users: [this.userId],
                        link: "/event/" + event._id,
                        eventId: event._id
                    });
        }
        //this is an open event and you weren't on the invite list, you can only attend or not attend
        else if (event.open) {
            if (responseString == 'attending') {
                Events.update({_id: options.eventId}, {$addToSet: {'attending' : this.userId}});
                addNotification([event.owner],
                    {
                        notificationType: notificationTypes.eventRSVP,
                        users: [this.userId],
                        link: "/event/" + event._id,
                        eventId: event._id
                    });
            }
            else
                Events.update({_id: options.eventId}, {$pull: {'attending' : this.userId}});
        }
    },

    //update an rsvp for an event
    updateLike : function(options) {
        check(options,{
            eventId : String,
            like : Boolean
        });

        var event = Events.findOne({_id: options.eventId});

        if (!event || ! this.userId || (!_.contains(event.invited, this.userId) && event.owner !== this.userId && !event.open))
            throw new Meteor.Error(403, "You cannot like this event");

        if (options.like) {
            Events.update({_id: options.eventId}, {$addToSet: {'like' : this.userId}});
            addNotification([event.owner],
                {
                    notificationType: notificationTypes.eventLike,
                    users: [this.userId],
                    link: "/event/" + event._id,
                    eventId: event._id
                });
        }
        else
            Events.update({_id: options.eventId}, {$pull: {'like' : this.userId}});
    },

    //add a comment
    addComment : function(options) {
        check(options,{
            eventId: String,
            comment: String
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var event = Events.findOne({_id: options.eventId});
        if (!event || (!event.open && !_.contains(event.invited, this.userId) && event.owner !== this.userId))
            throw new Meteor.Error(403, "No such event exists.");

        Events.update({_id:options.eventId}, {$push: {comments: {
            userId: this.userId,
            comment: options.comment,
            timeStamp: (new Date()).getTime(),
            likes: []
        }}});
        addNotification([event.owner],
            {
                notificationType: notificationTypes.eventComment,
                users: [this.userId],
                link: "/event/" + event._id,
                eventId: event._id
            });
    },

    removeComment : function(options) {
        check(options,{
            eventId: String,
            timeStamp: Number
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        Events.update(
            {_id: options.eventId},
            {$pull: {comments : {timeStamp: options.timeStamp, userId: this.userId}}}
        );
        return true;
    },

    likeComment : function(options) {
        check(options,{
            eventId: String,
            userId: String,
            timeStamp: Number,
            addRemove: Boolean
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var event = Events.findOne({_id: options.eventId});
        if (!event || (!event.open && !_.contains(event.invited, this.userId) && event.owner !== this.userId))
            throw new Meteor.Error(403, "No such event exists.");

        var index = _.indexOf(_.pluck(event.comments, 'timeStamp'), options.timeStamp);
        for (var i = index ; i < event.comments.length ; i++) {
            if (event.comments[i].timeStamp > options.timeStamp)
                break;
            else if (event.comments[i].userId == options.userId && event.comments[i].timeStamp == options.timeStamp)
            {
                if (Meteor.isServer) {
                    if (options.addRemove) {
                        Events.update(
                            {_id: options.eventId, "comments.userId": options.userId, "comments.timeStamp": options.timeStamp},
                            {$addToSet: {"comments.$.likes": this.userId}}
                        );
                        addNotification([options.userId],
                            {
                                notificationType: notificationTypes.eventCommentLike,
                                users: [this.userId],
                                link: "/event/" + event._id,
                                eventId: event._id,
                                commentUserId: options.userId,
                                commentTimeStamp: options.timeStamp
                            });
                    }
                    else {
                        Events.update(
                            {_id: options.eventId, "comments.userId": options.userId, "comments.timeStamp": options.timeStamp},
                            {$pull: {"comments.$.likes": this.userId}}
                        );
                    }
                }
                else {
                    var modifier;
                    if (options.addRemove) {
                        modifier = {$addToSet:{}};
                        modifier.$addToSet["comments." + i + ".likes"] = this.userId;
                        Events.update(options.eventId, modifier);
                    }
                    else {
                        modifier = {$pull:{}};
                        modifier.$pull["comments." + i + ".likes"] = this.userId;
                        Events.update(options.eventId, modifier);
                    }
                }
                return true;
            }
        }
        return false;
    },

    addPollTime: function(options) {
        check(options,{
            eventId: String,
            pollTime: String
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var event = Events.findOne({_id: options.eventId});
        if (!event || event.owner !== this.userId)
            throw new Meteor.Error(403, "No such event exists.");

        var dateTime = (new Date(options.pollTime)).getTime();
        if (dateTime && !_.contains(_.pluck(event.pollTime, "time"), dateTime)) {
            Events.update({_id:options.eventId}, {$addToSet: {pollTime: {
                time: dateTime,
                users: []
            }}});
            addNotification(event.open ? event.attending : event.invited,
                {
                    notificationType: notificationTypes.addEventPollTime,
                    users: [this.userId],
                    link: "/event/" + event._id,
                    eventId: event._id
                });
        }
    },

    deletePollTime: function(options) {
        check(options,{
            eventId: String,
            pollTime: Number
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var event = Events.findOne({_id: options.eventId});
        if (!event || event.owner !== this.userId)
            throw new Meteor.Error(403, "No such event exists.");

        Events.update({_id:options.eventId}, {$pull: {pollTime: {time: options.pollTime}}});
        addNotification(event.open ? event.attending : event.invited,
            {
                notificationType: notificationTypes.removeEventPollTime,
                users: [this.userId],
                link: "/event/" + event._id,
                eventId: event._id
            });
    },

    choosePollTime: function(options) {
        check(options,{
            eventId: String,
            pollTime: Number,
            isChecked: Boolean
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var event = Events.findOne({_id: options.eventId});
        if (!event || (!event.open && !_.contains(event.invited, this.userId) && event.owner !== this.userId))
            throw new Meteor.Error(403, "No such event exists.");

        var modifier;
        if (event.pollTime) {
            var index = _.indexOf(_.pluck(event.pollTime, 'time'), options.pollTime);
            if (index > -1) {
                if (options.isChecked) {
                    if (Meteor.isServer)
                        Events.update({_id:options.eventId, "pollTime.time": options.pollTime}, {$addToSet: {"pollTime.$.users": this.userId}});
                    else {
                        modifier = {$addToSet: {}};
                        modifier.$addToSet["pollTime." + index + ".users"] = this.userId;
                        Events.update({_id:options.eventId}, modifier);
                    }
                    addNotification([event.owner],
                        {
                            notificationType: notificationTypes.chooseEventPollTime,
                            users: [this.userId],
                            link: "/event/" + event._id,
                            eventId: event._id
                        });
                }
                else {
                    if (Meteor.isServer)
                        Events.update({_id:options.eventId, "pollTime.time": options.pollTime}, {$pull: {"pollTime.$.users": this.userId}});
                    else {
                        modifier = {$pull: {}};

                        modifier.$addToSet["pollTime." + index + ".users"] = this.userId;
                        Events.update({_id:options.eventId}, modifier);
                    }
                }
            }
        }
    }
});


getResponseString = function(rsvp) {
    if (rsvp == -1) {
        return "notAttending";
    }
    else if (rsvp == 0) {
        return "maybe";
    }
    else {
        return "attending";
    }
};

/*
    this function returns the query that will be used for search.
    used in both publishEvents.js and eventslib.js
 */
searchEventsQuery = function(query, options, userId) {
    var re = new RegExp(query, "i");
    var findQuery = {};
    findQuery["$and"] = [];
    findQuery["$and"].push({$or: [{owner: userId},
        {invited: userId} ,
        {open: true} ]});
    findQuery["$and"].push({$or: [{title: {$regex: re}},
        {description: {$regex: re}}]});
    if (options.categories.length > 0)
        findQuery["$and"].push({categories: {$in: options.categories}});

    /*
        code to find out if the time matches your constraints
     */
    var timeCheck = {};
    timeCheck["$or"] = [];
    //first check for one-time events
    var oneTime = {};
    oneTime["$and"] = [];
    oneTime["$and"].push({timeStart: {$gte: options.afterTime}});
    oneTime["$and"].push({timeStart: {$lte: options.beforeTime}});
    timeCheck["$or"].push(oneTime);

    //now check for recurring, this is harder, will have to create a separate entry for every day of your search
    var numberDays = numDays(options.afterTime, options.beforeTime);
    if (numberDays <= 0) {
        //query is over a single day so just need to add one thing
        var recTime = {};
        recTime["$and"] = [];
        recTime["$and"].push({recurring: dateToRegexRecString(options.afterTime, options.beforeTime)});
        recTime["$and"].push({HHMMStart: {$gte: dateToHHMM(options.afterTime)}});
        recTime["$and"].push({HHMMStart: {$lte: dateToHHMM(options.beforeTime)}});
        recTime["$and"].push({recurring: {$exists: true}});
        timeCheck["$or"].push(recTime);
    }
    else {
        //check how many days are in between when user wants, and find events that are in those days
        var dayAfterStart = new Date(options.afterTime);
        var dayBeforeEnd = new Date(options.beforeTime);
        dayAfterStart.setHours(0);
        dayAfterStart.setMinutes(0);
        dayAfterStart.setMilliseconds(0);
        dayAfterStart.setSeconds(0);
        dayBeforeEnd.setHours(0);
        dayBeforeEnd.setMinutes(0);
        dayBeforeEnd.setMilliseconds(0);
        dayBeforeEnd.setSeconds(0);
        dayAfterStart.setDate(dayAfterStart.getDate()+1);
        dayBeforeEnd.setDate(dayBeforeEnd.getDate()-1);

        if (dayBeforeEnd >= dayAfterStart) {
            timeCheck["$or"].push(
                {$and:[
                    {recurring: dateToRegexRecString(dayAfterStart, dayBeforeEnd)},
                    {recurring: {$exists: true}}
                ]}
            );
        }

        //now have a specific check for the start date and end date, unless the recurring check earlier included all days
        if (numberDays < 9) {
            var startTimeCheck = {};
            startTimeCheck["$and"] = [];
            startTimeCheck["$and"].push({recurring: dateToRegexRecString(options.afterTime, options.afterTime)});
            startTimeCheck["$and"].push({HHMMStart: {$gte: dateToHHMM(options.afterTime)}});
            startTimeCheck["$and"].push({recurring: {$exists: true}});
            var endTimeCheck = {};
            endTimeCheck["$and"] = [];
            endTimeCheck["$and"].push({recurring: dateToRegexRecString(options.beforeTime, options.beforeTime)});
            endTimeCheck["$and"].push({HHMMStart: {$lte: dateToHHMM(options.beforeTime)}});
            endTimeCheck["$and"].push({recurring: {$exists: true}});
            timeCheck["$or"].push(startTimeCheck);
            timeCheck["$or"].push(endTimeCheck);
        }
    }
    findQuery["$and"].push(timeCheck);
    if (options.location.length == 0)
        return findQuery;

    //mongodb doesn't support $and including a location query, so u move that out
    // for example if you're looking for {$and: [title: "test", location:{}]}
    //it becomes {{$and:[title:"test"]},location:{}}
    findQuery["location"] = locationQuery(options.location, options.distance);
    return findQuery;
};

var hasNotPassed = function(){
    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    return {$or: [{recurring: {$exists: true}}, {timeStart: {$gte: today}}]};
};

myEvents = function(userId) {
    return {$and:[hasNotPassed(),{$or: [{invited: userId}, {owner: userId}, {attending: userId}, {maybe: userId}, {notAttending: userId}]}]};
};

openEvents = function(location) {
    var distance = 10; //default find events within 10 miles of where you are
    var query = {};
    if (location.length > 0)
        query["location"] = locationQuery(location, distance);
    query["$and"] = [];
    query["$and"].push({"open": true});
    query["$and"].push(hasNotPassed());
    return query;
};

/*
    Returns the next time an event is occurring
 */
eventNextDate = function(event, today) {
    if (event.recurring) {
        var index = today.getDay();
        var count = 0;
        /*
        if (event.HHMMEnd) {
            //check if the event has already ended, if so, increment index
            var endHour = Math.floor(event.HHMMEnd/100);
            var endMinutes = event.HHMMEnd % 100;
            if (event.HHMMEnd > event.HHMMStart && (endHour < today.getHours() || (endHour == today.getHours() && endMinutes < today.getMinutes()))) {
                index++;
                count++;
            }
        }*/
        while (event.recurring.charAt(index) != '1') {
            index = (++index) % 7;
            count++;
            if (count == 7)
                return false;
        }
        var retDate = new Date(today);
        retDate.setDate(count + today.getDate());
        retDate.setHours(Math.floor(event.HHMMStart/100));
        retDate.setMinutes(event.HHMMStart % 100);
        retDate.setSeconds(0);
        retDate.setMilliseconds(0);
        return retDate;
    }
    return event.timeStart;
};

/*
    Gets a cursor and returns an array sorted in chronological order
*/
sortEvents = function(cursor) {
    var today = new Date();
    var retArray = _.sortBy(cursor.fetch(), function(evt) {return eventNextDate(evt, today);});
    cursor.rewind();
    return retArray;
};