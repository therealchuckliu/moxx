/*
 Email information
 */

Meteor.startup(function () {
    process.env.MAIL_URL = 'smtp://' + strings["generalEmail"].replace("@","%40")  + ':moxxteam@smtp.gmail.com:465';
    Accounts.emailTemplates.from = strings["name"] + " HQ <" + strings["generalEmail"] + ">";
    Accounts.emailTemplates.siteName = strings["name"];
    Accounts.emailTemplates.resetPassword.text = function(user, url){
        var array = url.split('/');
        return "To reset your password click on the link below \n\n"
            + Meteor.absoluteUrl()+'resetpassword/'+array[array.length-1];
    }
});

//email header for all moxx emails
var emailHeader = function() {
    return '<div class="row"><a href="' + Meteor.absoluteUrl() + '"> Moxx </a></div>';
};

//email footer
var emailFooter = function() {
    return '<div class="row">Sincerely, <br>' + strings["name"] + ' Team</div>';
};

//use this wrapper function for sending all emails, will include header and footer
//to or bcc may be string or array of strings (can be empty)
moxxEmail = function(to, bcc, subject, html) {
    Email.send({
        to: to,
        from: Accounts.emailTemplates.from,
        subject: subject,
        bcc: bcc,
        html: emailHeader() + '<p>' + html + '</p>' + emailFooter()
    });
};

sendVerificationFromEmail = function(userId, address) {
    var user = Meteor.users.findOne(userId);
    if (!user)
        throw new Error("Can't find user");
    // make sure we have a valid address
    if (!address || !_.contains(_.pluck(user.emails || [], 'address'), address))
        throw new Error("No such email address for user.");

    var tokenRecord = {
        token: Random.id(),
        address: address,
        when: +(new Date)};
    Meteor.users.update(
        {_id: userId},
        {$push: {'services.email.verificationTokens': tokenRecord}});

    var verifyEmailUrl = Accounts.urls.verifyEmail(tokenRecord.token);
    moxxEmail(address, [], Accounts.emailTemplates.verifyEmail.subject(user),
        '<p>' + verifyEmailText(user) + '<br>' +
        '<a href="' + verifyEmailUrl + '"> Verify </a>'
        + '</p>');
};

Accounts.emailTemplates.verifyEmail.subject = function (user) {
    return "Welcome to Moxx, " + user.profile.first_name;
};

var verifyEmailText = function (user) {
    if (user.profile.hasBrand)
        return "Start hosting events that people will want to be at!"
            + " To activate your account, simply click the link below:\n\n";
    else
        return "Start browsing the events you like by the brands you like!"
            + " To activate your account, simply click the link below:\n\n";
};