/*
 publishEvents.js

 Define what is published related to events to the client. This should be optimized so as not to overwhelm
 the client's cache when database grows through passing in session variables that define what's exactly needed
 from the server
 */

Meteor.startup(function() {
    Events._ensureIndex({location:"2dsphere"});
});

//return events with titles that match query
Meteor.publish("searchEvents", function(query, options, count, categories) {
    check(query, String);
    check(options, {categories:[String],afterTime:Date, beforeTime:Date, location:[Number], distance:Number});
    check(count, Number);
    //options still contains categories, this will be removed later on
    check(categories, [String]);
    /*var cursor = Events.find(searchEventsQuery(query, options, this.userId), {limit: count});
    var ids = [];
    cursor.forEach(function(event) {
        ids.push(event.owner);
    });*/
    var results;
    var cursor = Events.find(searchEventsQuery(query, options, this.userId));
    if (cursor.count() <= count) {
        results = sortEvents(cursor);
        this.added("hasAll", "searchEvents", {hasAll: true});
    }
    else {
        results = sortEvents(cursor).slice(0, count);
        this.added("hasAll", "searchEvents", {hasAll: false});
    }

    var ownerIds = _.pluck(results, "owner");
    var eventIds = _.pluck(results, "_id");

    return [Events.find({_id: {$in: eventIds}}, {fields: {comments:0}}), Meteor.users.find({_id: {$in: ownerIds}}, {fields: {_id:1, username:1}})];
});

//publishing all events you're related to, i.e. invited or owned or responded to
Meteor.publish("myEvents", function(options){
    //options indicate count for # of events returned and userId if you're looking for common events w/ another user
    check(options, Object);
    var count = 0;
    if (options.hasOwnProperty("count"))
        count = options.count;
    var cursor;
    var results;
    if (options.hasOwnProperty("userId")) {
        if (options.hasOwnProperty("myNetwork") && options.myNetwork) {
            var commonEventsQuery = {};
            commonEventsQuery["$and"] = [myEvents(this.userId), myEvents(options.userId)];
            cursor = Events.find(commonEventsQuery, {fields: {comments:0}});
        }
        else {
            this.ready();
            return;
        }
    }
    else
        cursor = Events.find(myEvents(this.userId), {fields: {comments:0}});

    if (cursor.count() <= count) {
        results = sortEvents(cursor);
        this.added("hasAll", "myEvents", {hasAll: true});
    }
    else {
        results = sortEvents(cursor).slice(0, count);
        this.added("hasAll", "myEvents", {hasAll: false});
    }

    var ownerIds = _.pluck(results, "owner");
    var eventIds = _.pluck(results, "_id");
    return [Events.find({_id: {$in: eventIds}}, {fields: {comments:0}}), Meteor.users.find({_id: {$in: ownerIds}}, {fields: {_id:1, username:1}})];
});

//publishing all events that are  open that i follow
Meteor.publish("openEvents", function(location, options) {
    check(location, [Number]);
    check(options, Object);
    var count = 0;
    if (options.hasOwnProperty("count"))
        count = options.count;
    var cursor;
    var results;
    var query = openEvents(location);
    query["$and"].push({owner: {$in: myNetwork(this.userId)}});
    //only show events that you're allowed to see
    if (options.hasOwnProperty("userId")) {
        if (options.hasOwnProperty("myNetwork") && options.myNetwork) {
            //find open events the other person is attending or owns
            query["$and"].push(myEvents(options.userId));
            cursor = Events.find(query, {limit: count, fields: {comments:0}});
        }
        else {
            this.ready();
            return;
        }
    }
    else
        cursor = Events.find(query, {limit: count, fields: {comments:0}});

    if (cursor.count() <= count) {
        results = sortEvents(cursor);
        this.added("hasAll", "openEvents", {hasAll: true});
    }
    else {
        results = sortEvents(cursor).slice(0, count);
        this.added("hasAll", "openEvents", {hasAll: false});
    }
    var ownerIds = _.pluck(results, "owner");
    var eventIds = _.pluck(results, "_id");
    return [Events.find({_id: {$in: eventIds}}, {fields: {comments:0}}), Meteor.users.find({_id: {$in: ownerIds}}, {fields: {_id:1, username:1}})];
});

//get event by id
Meteor.publish("getEventInfo", function(id, commentCount){
    check(id,String);
    check(commentCount, Number);
    var eventData = Events.findOne({$and: [{_id : id}, {$or: [{owner: this.userId}, {open: true}, {invited: this.userId}]}]});
    if (eventData) {
        var numComments = eventData.comments.length;
        commentCount = commentCount > numComments ? numComments : commentCount;
        var showComments;
        if (commentCount == numComments) {
            showComments = eventData.comments;
            this.added("hasAll", "getEventInfo", {hasAll: true});
        }
        else {
            showComments = eventData.comments.slice(numComments - commentCount, numComments);
            this.added("hasAll", "getEventInfo", {hasAll: false});
        }
        var userIds = _.union([eventData.owner],_.pluck(showComments, 'userId'),
                                eventData.attending, eventData.maybe, eventData.invited,
                                eventData.notAttending, _.union.apply(_, _.pluck(showComments, 'likes')));

        var fields = {};
        fields["fields"] = {};
        fields["fields"]["comments"] = {$slice: commentCount*-1 };
        if (eventData.owner === this.userId){
            userIds = _.union(userIds, eventData.suggested);
        }
        else
            fields["fields"]["suggested"] = 0;
        var userData = Meteor.users.find({_id: {$in: userIds}}, {fields: consumerPublicInformation});

        return [Events.find({$and: [{_id : id}, {$or: [{owner: this.userId}, {open: true}, {invited: this.userId}]}]}, fields), userData];
    }
    this.ready();
});