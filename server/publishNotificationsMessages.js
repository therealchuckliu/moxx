/*
    Publish users and events in your notifications
    Publish data for moxxboxx
*/

Meteor.publish("myNotifications", function(notificationCount) {
    check(notificationCount, Number);
    var profile = Meteor.users.findOne({_id: this.userId});
    if (profile && profile.notifications) {
        var notifications = profile.notifications;
        var events = [];
        var chats = [];
        var users = [];
        for (var i = 0 , l = notifications.length < notificationCount ? notifications.length : notificationCount ;
             i < l ; i++) {
            if (notifications[notifications.length - i - 1].eventId)
                events.push(notifications[notifications.length - i - 1].eventId);
            if (notifications[notifications.length - i - 1].chatId)
                chats.push(notifications[notifications.length - i - 1].chatId);
            users = _.union(users, notifications[notifications.length - i - 1].users);
        }
        if (notifications.length <= notificationCount)
            this.added("hasAll", "myNotifications", {hasAll: true});
        else
            this.added("hasAll", "myNotifications", {hasAll: false});
        return [Meteor.users.find({_id: {$in: users}}, {fields: {username:1, profile:1}}),
                Events.find({_id: {$in: events}}, {fields: {title:1}}),
                MoxxBoxx.find({_id: {$in: chats}}, {fields: {title:1}})];
    }
    this.ready();
});

Meteor.publish("myNotificationsArray", function(notificationCount) {
   check(notificationCount, Number);
   return Meteor.users.find({_id: this.userId}, {fields: { notifications: {$slice: notificationCount*-1 } }} );
});

Meteor.publish("getChat", function(chatId, messageCount) {
    check(chatId, String);
    check(messageCount, Number);
    var chat = MoxxBoxx.findOne({_id: chatId, members: this.userId});
    if (chat) {
        if (!chat.messages || chat.messages.length <= messageCount)
            this.added("hasAll", "getChat", {hasAll: true});
        else
            this.added("hasAll", "getChat", {hasAll: false});
        return [Meteor.users.find({_id: {$in: chat.members}}, {fields: {username:1}}),
                MoxxBoxx.find({_id: chatId}, {fields: { messages: {$slice: messageCount*-1 } }} )];
    }
    this.ready();
});

Meteor.publish("myMoxxBoxx", function() {
    var ids = [];
    if (this.userId) {
        var myChats = MoxxBoxx.find({members: this.userId}, {fields: {messages: {$slice: -1}}});
        myChats.forEach(function(chat) {
            ids = _.union(ids, chat.members);
        });

        return [
            myChats,
            Meteor.users.find({_id: {$in: ids}}, {fields: {username:1}})
        ]
    }
    this.ready();
});
