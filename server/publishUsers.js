/*
 publishUsers.js

 -  Define what is published from users to the client. This should be optimized so as not to overwhelm
 the client's cache when database grows through passing in session variables that define what's exactly needed
 from the server
 */

//this is information that is public for a consumer that anyone can see
consumerPublicInformation = {username: 1,
                             profile: 1
                            };

privateInformation = {emails:0,
                      notifications:0,
                      services:0,
                      consumerReqSent:0,
                      consumerReqReceived:0,
                      brandReqSent:0,
                      brandReqReceived:0,
                      newNotifCount: 0
                     };

//startup create index for location
Meteor.startup(function() {
    Meteor.users._ensureIndex({'profile.location':"2dsphere"});
});

/*
    admin publish for administrators only
 */
Meteor.publish("admin", function() {
    var prof = Meteor.users.findOne({_id: this.userId});

    if (prof && adminEmail(prof.emails[0].address) && prof.emails[0].verified) {
        return [Meteor.users.find(), Events.find()];
    }
    this.ready();
});

/*
 get users that have requested to follow you
 */
Meteor.publish("myFollowRequests", function() {
    var prof = Meteor.users.findOne({_id: this.userId});
    if(prof){
        var consumerReq = prof.consumerReqReceived == undefined ? [] : prof.consumerReqReceived;
        var brandReq = prof.brandReqReceived == undefined ? [] : prof.brandReqReceived;
        var arr = _.union(consumerReq,brandReq);
        return Meteor.users.find({_id: {$in : arr}},
                {fields:consumerPublicInformation});
    }
    this.ready();
});

/*
    get users that match certain query
 */
Meteor.publish("searchUsers", function(query, isBrand, options, count, categories) {
    check(query, String);
    check(isBrand, Boolean);
    check(count, Number);
    check(options, {location:[Number], distance: Number, categories:[String]});
    //categories will need to be removed from options later on
    check(categories, [String]);

    if (query.length > 0 || isBrand) {
        var fieldsQuery = {};
        fieldsQuery["fields"] = consumerPublicInformation;
        fieldsQuery["limit"] = count;
        var cursor = Meteor.users.find(searchUserQuery(query, isBrand, options),
            //search for matching first name, last name, username
            //only return those values + prof pic and following info, but nothing else
            fieldsQuery);
        if (cursor.count() <= count)
            this.added("hasAll", "searchUsers", {hasAll: true});
        else
            this.added("hasAll", "searchUsers", {hasAll: false});

        return cursor;
    }
    this.ready();
});

/*
    get users that username is following
 */
Meteor.publish("getUserFollowing", function(username, getBrand) {
    check(username, String);
    check(getBrand, Boolean);
    var query = userConnections(username, getBrand, "Following");
    if (query) {
        return Meteor.users.find(query,
                {fields: consumerPublicInformation});
    }
    this.ready();
});

/*
    get users that are following username
 */
Meteor.publish("getUserFollowers", function(username, getBrand) {
    check(username, String);
    check(getBrand, Boolean);
    var query = userConnections(username, getBrand, "Followers");
    if (query) {
        return Meteor.users.find(query,
            {fields: consumerPublicInformation});
    }
    this.ready();
});

//get info of specific user
Meteor.publish("getUserByUsername", function(username) {
    check(username, String);
    var me = Meteor.users.findOne({_id: this.userId});
    var id = Meteor.users.findOne({username: {$regex: new RegExp("^" + username.toLowerCase() + "$", "i")}});
    if (id && me) {
        var arr = me.consumerFollowing ? me.consumerFollowing : [];
        var isBrand = id.profile.hasBrand;
        id = id._id;
        if (id == me._id)
            return Meteor.users.find({_id: id});
        return _.contains(arr, id) || isBrand ?
            Meteor.users.find({_id: id},
                {fields:privateInformation})
            :
            Meteor.users.find({_id: id},
                //only return this info about who this user is following
                {fields:consumerPublicInformation});
    }
    else
        this.ready();
});

Meteor.publish("getUserById", function(id) {
    check(id, String);
    var me = Meteor.users.findOne({_id: this.userId});
    var isBrand = Meteor.users.findOne({_id: id});
    if (me && isBrand) {
        var arr = me.consumerFollowing ? me.consumerFollowing : [];
        isBrand = isBrand.profile.hasBrand;
        if (id == this.userId)
            return Meteor.users.find({_id: id});
        return _.contains(arr, id) || isBrand ?
            Meteor.users.find({_id: id},
                {fields:privateInformation})
            :
            Meteor.users.find({_id: id},
                //only return this info about who this user is following
                {fields:consumerPublicInformation});
    }
    else
        this.ready();
});

