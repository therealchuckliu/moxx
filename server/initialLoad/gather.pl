#!/usr/bin/perl
use strict;
use warnings;
##
#Gather Bars from New-York, written by Mohit Raghunathan
#
#usage: gather.pl <locationType> <Offset>
#
use LWP::UserAgent 6;
use LWP::Simple;
use Net::OAuth;
$Net::OAuth::PROTOCOL_VERSION = Net::OAuth::PROTOCOL_VERSION_1_0;
use HTTP::Request::Common;
use JSON qw(decode_json encode_json);
use Data::Dumper;

my $ua = LWP::UserAgent->new;

sub consumer_key { 'BL6ZT1ywl62P74hnsVo8Qw' }
sub consumer_secret { 'uNhfA9pPqse4kehPiJ_G2P-JeeM' }
sub access_token { 'Xaz970EWQfI9nVubRTU-cHTyYOoy3CWD' }
sub access_token_secret { 'ImynPwvC-ss3odEGZTj4bYX0Jfo' }

sub user_url { "http://api.yelp.com/v2/search?term=$ARGV[0]&limit=10&location=New+York&offset=$ARGV[1]" }

my $request =
        Net::OAuth->request('protected resource')->new(
          consumer_key => consumer_key(),
          consumer_secret => consumer_secret(),
          token => access_token(),
          token_secret => access_token_secret(),
          request_url => user_url(),
          request_method => 'GET',
          signature_method => 'HMAC-SHA1',
          timestamp => time,
          nonce => nonce(),
        );

$request->sign;
my $res = $ua->request(GET $request->to_url);
#print ">>START JSON<<\n";
#print $res->content;
#print ">>END JSON<<\n";
my $decode_json = decode_json($res->content);
if ($res->is_success) {
  print "Success\n";
} else {
  die "Something went wrong";
}

sub nonce {
  my @a = ('A'..'Z', 'a'..'z', 0..9);
  my $nonce = '';
  for(0..31) {
    $nonce .= $a[rand(scalar(@a))];
  }

  $nonce;
}

## ADJUST TO MOXX FORMAT ##
sub adjustToMoxx
{
  my $bus = $decode_json->{"businesses"};
  my %storeHash;
  foreach my $arr(@{$bus})
  {
    
    my $name = $arr->{"name"};
    my $website = $arr->{"url"};
    my $phoneNumber = $arr->{"phone"};
    my $address = $arr->{"location"}->{"address"}->[0];
    my $zipcode = $arr->{"location"}->{"postal_code"};
    my $combadd = "$address $zipcode";
    my ($long,$lat) = getLatLong($combadd);
    $storeHash{"$name"} = {'business_name'=>$name, 'website'=>$website,'phoneNumber'=>$phoneNumber,"zipcode"=>$zipcode,"location"=>[$long,$lat]};
}
  my $json = encode_json(\%storeHash);
  print $json,"\n";

}


sub getLatLong($){
  
  my ($address) = @_;
  my $format = "json"; #can also to 'xml'
  my $geocodeapi = "http://maps.googleapis.com/maps/api/geocode/";
  my $url = $geocodeapi . $format . "?sensor=false&address=" . "\"".$address."\"";
  print "$url\n";
  my $ua = LWP::UserAgent->new();
  my $req = new HTTP::Request GET => $url;
  my $json = $ua->request($req)->content; die "couldn't parse address for geocoding" unless defined $json;
  my $d_json = decode_json( $json );
  my $lat = $d_json->{results}->[0]->{geometry}->{location}->{lat};
  my $lng = $d_json->{results}->[0]->{geometry}->{location}->{lng};
  #print "ADDRESS=$address,LAT=$lat,LONG=$lng\n";
  return ($lng, $lat);
}

adjustToMoxx();




