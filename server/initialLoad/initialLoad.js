Meteor.methods({
    loadInit: function() {
        var init = JSON.parse(Assets.getText('scraped.json'));
        _.each(init, function(it) {
            var brand = it["Brand"];
            var events = it["Events"];
            if (brand.zipcode.length != 5)
                throw new Meteor.Error(403, brand.business_name + " has an invalid zipcode.");
            if (brand.phoneNumber.length != 10)
                throw new Meteor.Error(403, brand.business_name + " has an invalid phone #. just provide a 10-digit number in quotes.");
            if (brand.location.length != 2)
                throw new Meteor.Error(403, brand.business_name + " has invalid longitude/latitude coordinates.");
            var prof = Meteor.users.findOne({"profile.business_name": brand.business_name});
            var userId;
            if (!prof) {
                userId = Accounts.createUser({
                    profile: {
                        hasBrand : true,
                        business_name : brand.business_name,
                        website: brand.website.replace('http://', ''),
                        phoneNumber: brand.phoneNumber,
                        address: brand.address + " " + brand.zipcode,
                        location: {type: "Point",
                            coordinates: brand.location},
                        categories: brand.categories,
                        uploadedpic: [{
                            "src": brand.pic,
                            "caption": ""
                        }]
                    },
                    username: brand.business_name.replace(/\W/g, '')
                });
            }
            else {
                userId = prof._id;
                var query = {};
                query["$set"] = {};
                query["$set"]["profile.website"] = brand.website.replace('http://', '');
                query["$set"]["profile.phoneNumber"] = brand.phoneNumber;
                query["$set"]["profile.address"] = brand.address + " " + brand.zipcode;
                query["$set"]["profile.location"] = {type: "Point",
                    coordinates: brand.location};
                query["$set"]["profile.categories"] = brand.categories;
                query["$unset"] = {};
                query["$unset"]["profile.zipcode"] = "";
                query["$set"]["profile.uploadedpic"] = [{"src": brand.pic, "caption":""}]
                Meteor.users.update({_id: userId}, query);
            }

            _.each(events, function(event) {
                var eventProf = Events.findOne({owner: userId, title: event.title});
                if (!eventProf) {
                    if (event.HHMMEnd)
                        Events.insert({
                            active: true,
                            owner: userId,
                            uploadedpic: brand.pic,
                            title: event.title,
                            tagline: event.tagline,
                            description: event.description.replace(/\n/g, '<br />'),
                            address: brand.address + " " + brand.zipcode,
                            location: {type: "Point",
                                coordinates: brand.location},
                            open: true,
                            invited: [],
                            suggested: [],
                            attending: [userId],
                            maybe:[],
                            notAttending: [],
                            categories: brand.categories,
                            like: [],
                            comments: [],
                            HHMMStart: event.HHMMStart,
                            HHMMEnd: event.HHMMEnd,
                            recurring: event.recurring
                        });
                    else
                        Events.insert({
                            active: true,
                            owner: userId,
                            uploadedpic: brand.pic,
                            title: event.title,
                            tagline: event.tagline,
                            description: event.description.replace(/\n/g, '<br />'),
                            address: brand.address + " " + brand.zipcode,
                            location: {type: "Point",
                                coordinates: brand.location},
                            open: true,
                            invited: [],
                            suggested: [],
                            attending: [userId],
                            maybe:[],
                            notAttending: [],
                            categories: brand.categories,
                            like: [],
                            comments: [],
                            HHMMStart: event.HHMMStart,
                            recurring: event.recurring
                        });
                }
                else {
                    var query = {};
                    query["$set"] = {};
                    query["$set"]["tagline"] = event.tagline;
                    query["$set"]["description"] = event.description.replace(/\n/g, '<br />');
                    query["$set"]["address"] = brand.address + " " + brand.zipcode;
                    query["$set"]["location"] = {type: "Point",
                        coordinates: brand.location};
                    query["$set"]["HHMMStart"] = event.HHMMStart;
                    if (event.HHMMEnd)
                        query["$set"]["HHMMEnd"] = event.HHMMEnd;
                    query["$set"]["recurring"] = event.recurring;
                    query["$set"]["categories"] = brand.categories;
                    query["$set"]["uploadedpic"] = brand.pic;
                    Events.update({_id: eventProf._id}, query);
                }
            });
        });
    }
});