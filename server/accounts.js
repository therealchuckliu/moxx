/*
    Accounts

    -  Handle account creation and facebook authentication
    -  Email handling
*/

//Accounts.loginServiceConfiguration.remove({
//    service: "facebook"
//});
//
//Accounts.loginServiceConfiguration.insert({
//    service: "facebook",
//    appId: "499627856794184",
//    secret: "188e2f45300ede8f56599a1a378cdbd2"
//});

Accounts.onCreateUser(function(options, user){
    if (user.services.facebook != undefined) {
        var profile;

        profile = {"first_name": user.services.facebook.first_name, "last_name": user.services.facebook.last_name};
        user.profile = profile;
        user.emails = [{"address": user.services.facebook.email, "verified" : true}];
    }
    else {
        user.profile = options.profile;
    }
    return user;
});

Accounts.config({forbidClientAccountCreation: true});