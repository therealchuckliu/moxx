/*
    Code related to reststop2 package
    Also any meteor methods needed for rest api
*/

var paramToHash = function(params) {
    var options = {};
    var paramsArr = params.split("&");
    for (var i = 0 ; i < paramsArr.length ; ++i) {
        var queryArr = paramsArr[i].split("=");
        if (queryArr.length == 2)
            options[queryArr[0]] = queryArr[1];
    }
    return options;
};

RESTstop.configure({
    use_auth: true
});

RESTstop.add('get_user/:id', {require_login: true}, function() {
    if(!this.user) {
        return {'success': false};
    }
    var profile = RESTstop.getPublished(this, 'getUserById', [this.params.id]);
    if (profile.count() > 0) {
        return (profile.fetch())[0];
    }
    return {'success': false};
});

RESTstop.add('getFollowing/:id', {require_login: true}, function() {
    if (!this.user) {
        return {'success' : false};
    }
    return RESTstop.call(this, 'getConnections', this.params.id, 'Following');
});

RESTstop.add('getFollowers/:id', {require_login: true}, function() {
    if (!this.user) {
        return {'success' : false};
    }
    return RESTstop.call(this, 'getConnections', this.params.id, 'Followers');
});

RESTstop.add('getEvents/:id/:options', {require_login: true}, function() {
    if (!this.user) {
        return {'success' : false};
    }
    return RESTstop.call(this, 'getProfEvents', this.params.id, paramToHash(this.params.options), true);
});

RESTstop.add('getHome/:options', {require_login: true}, function() {
    if (!this.user) {
        return {'success' : false};
    }
    return RESTstop.call(this, 'getProfEvents', this.user._id, paramToHash(this.params.options), false);
});

Meteor.methods({
    getConnections : function(id, followText) {
        check(id, String);
        check(followText, String);
        var prof = Meteor.users.findOne({_id: id});
        if (prof) {
            var connections = _.union(prof['brand' + followText], prof['consumer' + followText]);
            return {"connections": Meteor.users.find({_id: {$in: connections}}).fetch()};
        }
        return {"connections": []};
    },

    getProfEvents: function(id, options, myOpen) {
        check(id, String);
        check(options, Object);
        check(myOpen, Boolean);

        var count = options.count ? options.count : 0;
        var cursor;
        var results;
        if (id == this.userId || inMyNetwork(id, this.userId)) {
            if (id == this.userId)
                cursor = Events.find(myEvents(this.userId), {fields: {comments:0}});
            else {
                var commonEventsQuery = {};
                commonEventsQuery["$and"] = [myEvents(this.userId), myEvents(id)];
                cursor = Events.find(commonEventsQuery, {fields: {comments:0}});
            }
            results = sortEvents(cursor).slice(0, count);
            var query = openEvents(options.location ? options.location : []);
            query["$and"].push({owner: {$in: myNetwork(this.userId)}});
            if (myOpen)
                query["$and"].push(myEvents(id));
            cursor = Events.find(query, {limit: count, fields: {comments:0}});
            var containedIds = _.pluck(results, "_id");
            var resultsTemp = sortEvents(cursor).slice(0, count);
            for (var i = 0 ; i < resultsTemp.length ; i++) {
                if (!_.contains(containedIds, resultsTemp[i]._id))
                    results.push(resultsTemp[i]);
            }
            for (var i = 0 ; i < results.length ; i++) {
                results[i]["owner"] = Meteor.users.findOne({_id: results[i]["owner"]}).username
            }
            return {"resultCount": results.length, "events": _.sortBy(results, function(evt) {return eventNextDate(evt, new Date());})};
        }
        return {"events": [], "resultCount" : 0};
    }
});