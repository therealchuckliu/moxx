/*
    Server functions for creating users, brands, events
*/

Meteor.methods({

    //send verification email
    adminVerification: function() {
        this.unblock();
        var prof = Meteor.users.findOne({_id: this.userId});
        if (prof && adminEmail(prof.emails[0].address) && !prof.emails[0].verified && !prof.services.email)  {
            this.unblock();
            sendVerificationFromEmail(this.userId, prof.emails[0].address);
        }
    },

    sendAdminEmail: function(options) {
        check(options, {
            to: [String],
            bcc: [String],
            subject: String,
            body: String
        });
        this.unblock();
        var prof = Meteor.users.findOne({_id: this.userId});
        if (prof && adminEmail(prof.emails[0].address) && prof.emails[0].verified)  {
            this.unblock();
            moxxEmail(options.to, options.bcc, options.subject, options.body);
        }
    },

    setNewPassword: function(options){
        check(options,{
            id: String,
            newPassword: String
        });
        Accounts.setPassword(options.id, options.newPassword);
    },

    // Create Brand //
    createBrand: function (options) {
        check(options, {
            first_name: String,
            last_name: String,
            business_name: String,
            website: String,
            phoneNumber: String,
            address: String,
            email : String,
            password : String,
            location: [Number] //[longitude,latitude]
        });
        if (Meteor.users.find({'emails.address' : {$regex: new RegExp("^" + options.email.toLowerCase() + "$", "i")}}).count() > 0)
            throw new Meteor.Error(404, "Account with email already exists. Use another email address");


        var userId = Accounts.createUser(
            {
                email: options.email,
                password: options.password,
                notifications: [],       //notifications schema {message (string), link (string), timestamp (datetime)}
                consumerFollowing: [], //consumers who you are following
                consumerFollowers: [], //consumers who are following you
                brandFollowing: [],    //brands who you are following
                brandFollowers: [],    //brands who are following you
                consumerReqSent: [],
                consumerReqReceived: [],
                brandReqSent: [],
                brandReqReceived: [],
                profile: {
                    hasBrand: true,
                    first_name: options.first_name.formatName(),
                    last_name: options.last_name.formatName(),
                    categories: [],
                    uploadedpic: [],
                    business_name: options.business_name.formatName(),
                    website: options.website.replace('http://', ''),
                    phoneNumber: options.phoneNumber,
                    address: options.address,
                    location: {type: "Point", //setup for mongodb 2dsphere indexes
                        coordinates: options.location}
                },
                savedLocations: [{
                    address: options.address,
                    zipcode: options.zipcode,
                    location: {type: "Point", coordinates: options.location}
                }],
                status: "",
                newNotifCount: 0
            }
        );
        this.unblock();
        //sendVerificationFromEmail(userId, options.email);
    },

    // Create Consumers //
    createConsumer: function (options) {
        check(options, {
            first_name : String,
            last_name : String,
            email : String,
            password: String
        });
        if (Meteor.users.find({'emails.address' : {$regex: new RegExp("^" + options.email.toLowerCase() + "$", "i")}}).count() > 0)
            throw new Meteor.Error(404, "Account with email already exists. Use another email address");

        var userId = Accounts.createUser(
            {
                email: options.email,
                password: options.password,
                notifications: [],       //notifications schema {message (string), link (string), timestamp (datetime)}
                consumerFollowing: [], //consumers who you are following
                consumerFollowers: [], //consumers who are following you
                brandFollowing: [],    //brands who you are following
                brandFollowers: [],    //brands who are following you
                consumerReqSent: [],
                consumerReqReceived: [],
                brandReqSent: [],
                brandReqReceived: [],
                profile: {
                    hasBrand: false,
                    first_name: options.first_name.formatName(),
                    last_name: options.last_name.formatName(),
                    categories: [],
                    uploadedpic: []
                },
                savedLocations: [], //a blank array isn't created on startup, you will need to check if this exists from client side
                status: "",
                newNotifCount: 0
            }
        );
        this.unblock();
        //sendVerificationFromEmail(userId, options.email);
    },

    //finish registration
    completeRegistration: function(options) {
        check(options, {
            username: String,
            uploadedpic: String,
            caption: String
        });

        if (Meteor.users.find({'username' : {$regex: new RegExp("^" + options.username.toLowerCase() + "$", "i")}}).count() > 0)
            throw new Meteor.Error(404, "Account with that username was just created. Use another username.");

        Meteor.users.update({_id:this.userId}, {$set: {username: options.username},
            $push: {'profile.uploadedpic': {src: options.uploadedpic, caption: options.caption}}});

        return true;
    },

    updateSavedLocation: function(options) {
        check(options, {
            address: String,
            zipcode: Number,
            location: [Number],
            addRemove: Boolean
        });

        if (!this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");
        var updateQuery = {};
        if (options.addRemove)
            updateQuery["$push"] = {
                savedLocations: {
                address: options.address,
                zipcode: options.zipcode,
                location: {type: "Point", coordinates: options.location}
            }};
        else
            updateQuery["$pull"] = {
                savedLocations: {
                    address: options.address,
                    zipcode: options.zipcode,
                    location: {type: "Point", coordinates: options.location}
                }};
        Meteor.update({_id: this.userId}, updateQuery);
    },

    //triggered when user edits categories/interested
    updateCategory : function(options){
        check(options,{
            category : String,
            removeFromDB : Boolean
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        if(options.removeFromDB){
            Meteor.users.update({_id: this.userId}, {$pull: {'profile.categories' : options.category}});
        }else{
            Meteor.users.update({_id: this.userId}, {$addToSet: {'profile.categories' : options.category}});
        }
    },

    // Update user info
    updateInfo: function(options) {
        check(options,{
            field: String,
            value: Match.Any
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        if (options.field.toLowerCase().indexOf('email') != -1) {
            if (Meteor.users.find({'emails.address' : {$regex: new RegExp("^" + options.value.toLowerCase() + "$", "i")}}).count() == 0) {
                Meteor.users.update({_id: this.userId}, {$set: {'emails' : [{address: options.value, verified: false}]}});
            }
            else {
                options.field = "";
            }
            return options;
        }
        else if (options.field.toLowerCase().indexOf('username') != -1) {
            if (Meteor.users.find({'username' : {$regex: new RegExp("^" + options.value.toLowerCase() + "$", "i")}}).count() == 0) {
                if (options.value.replace(/[a-z0-9]+$/ig, "") != "" || options.value.length == 0) {
                    throw new Meteor.Error(403, "You must enter a username with only alphanumeric letters");
                }
                Meteor.users.update({_id: this.userId}, {$set: {'username' : options.value}});
            }
            else {
                options.field = "";
            }
            return options;
        }
        else {
            //you can dynamically create queries
            //you have to create an object of 2 levels, first is the operation, second is the field -> value
            var whatToUpdate = {};
            whatToUpdate["$set"] = {};
            whatToUpdate["$set"]["profile." + options.field] = options.value;
            Meteor.users.update({_id: this.userId}, whatToUpdate);
            return options;
        }
    },

    //add following
    updateMyFollowers : function(options){
        check(options,{
            id : String,
            addRemove: Boolean //true to add, false to remove
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var amIBrand = Meteor.users.findOne({_id: this.userId});
        var isHeBrand = Meteor.users.findOne({_id: options.id});

        if (amIBrand && isHeBrand) {
            var myUsername = amIBrand.username;
            amIBrand = amIBrand.profile.hasBrand;
            isHeBrand = isHeBrand.profile.hasBrand;
            var myUpdate = {};
            var hisUpdate = {};
            var addorpull = options.addRemove ? "$addToSet" : "$pull";

            myUpdate[addorpull] = {};
            hisUpdate[addorpull] = {};
            var myUpdateString = isHeBrand ? 'brandFollowers' : 'consumerFollowers';
            var hisUpdateString = amIBrand ? 'brandFollowing' : 'consumerFollowing';
            myUpdate[addorpull][myUpdateString] = options.id;
            hisUpdate[addorpull][hisUpdateString] = this.userId;
            var pullString;
            //remove the request from the other user's reqSent
            pullString = amIBrand ? 'brandReqSent' : 'consumerReqSent';
            if (addorpull !== "$pull")
                hisUpdate["$pull"] = {};
            hisUpdate["$pull"][pullString] = this.userId;

            //remove reqReceived from my profile
            pullString = isHeBrand ? 'brandReqReceived' : 'consumerReqReceived';
            if (addorpull !== "$pull")
                myUpdate["$pull"] = {};
            myUpdate["$pull"][pullString] = options.id;


            Meteor.users.update({_id: options.id}, hisUpdate);
            Meteor.users.update({_id: this.userId}, myUpdate);
            if (options.addRemove) {
                addNotification([options.id],
                    {
                        notificationType: notificationTypes.following,
                        users: [this.userId],
                        link: "/profile/" + myUsername
                    });
            }
        }

        return true;
    },

    //unfollow
    updateMyFollowing: function(options) {
        check(options,{
            id : String,
            addRemove: Boolean
        });
        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var amIBrand = Meteor.users.findOne({_id: this.userId});
        var isHeBrand = Meteor.users.findOne({_id: options.id});

        if (amIBrand && isHeBrand) {
            amIBrand = amIBrand.profile.hasBrand;
            isHeBrand = isHeBrand.profile.hasBrand;
            var myUpdate = {};
            var hisUpdate = {};
            var updateString = options.addRemove ? "$addToSet" : "$pull";
            myUpdate[updateString] = {};
            hisUpdate[updateString] = {};
            var myUpdateString = isHeBrand ? 'brandFollowing' : 'consumerFollowing';
            var hisUpdateString = amIBrand ? 'brandFollowers' : 'consumerFollowers';
            myUpdate[updateString][myUpdateString] = options.id;
            hisUpdate[updateString][hisUpdateString] = this.userId;
            Meteor.users.update({_id: options.id}, hisUpdate);
            Meteor.users.update({_id: this.userId}, myUpdate);
        }

        return true;
    },

    //add following request
    updateFollowingRequest : function(options){
        check(options,{
            id : String,
            addRemove: Boolean //true to add, false to remove
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        var amIBrand = Meteor.users.findOne({_id: this.userId});
        var isHeBrand = Meteor.users.findOne({_id: options.id});

        if (amIBrand && isHeBrand) {
            var myUsername = amIBrand.username;
            amIBrand = amIBrand.profile.hasBrand;
            isHeBrand = isHeBrand.profile.hasBrand;
            var myUpdate = {};
            var hisUpdate = {};
            var addorpull = options.addRemove ? "$addToSet" : "$pull";
            myUpdate[addorpull] = {};
            hisUpdate[addorpull] = {};
            var myUpdateString = isHeBrand ? 'brandReqSent' : 'consumerReqSent';
            var hisUpdateString = amIBrand ? 'brandReqReceived' : 'consumerReqReceived';
            myUpdate[addorpull][myUpdateString] = options.id;
            hisUpdate[addorpull][hisUpdateString] = this.userId;
            Meteor.users.update({_id: options.id}, hisUpdate);
            Meteor.users.update({_id: this.userId}, myUpdate);
            if (options.addRemove) {
                addNotification([options.id],
                    {
                        notificationType: notificationTypes.followingrequest,
                        users: [this.userId],
                        link: "/profile/" + myUsername
                    });
            }
        }

        return true;
    },

    updateStatus : function(options) {
        check(options,{
            status: String
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        Meteor.users.update({_id: this.userId}, {$set: {status: options.status}});
    },

    //check if username is valid
    validUsername: function(options) {
        check(options, {
            username: String
        });
        return (Meteor.users.find({'username' : {$regex: new RegExp("^" + options.username.toLowerCase() + "$", "i")}}).count() == 0);
    },

    //grab username or email w/ correct case
    correctCase: function(options) {
        check(options, {
            userInfo: String
        });
        var caseUserInfo = "hello";

        if (validEmail(options.userInfo)) {
            var prof = Meteor.users.findOne({'emails.address': {$regex: new RegExp("^" + options.userInfo.toLowerCase() + "$", "i")}});
            if (prof)
                caseUserInfo = prof.emails[0].address;
        }
        else {
            var prof = Meteor.users.findOne({username: {$regex: new RegExp("^" + options.userInfo.toLowerCase() + "$", "i")}});
            if (prof)
                caseUserInfo = prof.username;
        }
        return caseUserInfo;
    },

    //add a profile picture
    addProfPic : function(options) {
        check(options,{
            uploadedpic : String,
            caption : String
        });

        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in to do this.");

        Meteor.users.update({_id:this.userId}, {$push: {'profile.uploadedpic': {src: options.uploadedpic, caption: options.caption}}});
    }

});

/*
    query to find users, used in both publishusers.js and userslib.js
 */

searchUserQuery = function(query, isBrand, options) {
    var re = new RegExp(query, "i");
    var findQuery = {};
    findQuery["$and"] = [];
    findQuery["$and"].push({$or:
        [
            {'profile.first_name': {$regex: re}},
            {'profile.last_name': {$regex: re}} ,
            {username: {$regex: re}}
        ]
    });
    findQuery["$and"].push({'profile.hasBrand':isBrand});
    if (isBrand) {
        if (options.categories.length > 0)
            findQuery["$and"].push({'profile.categories': {$in: options.categories}});
        if (options.location.length > 0)
            findQuery['profile.location'] = locationQuery(options.location, options.distance);
    }
    return findQuery;
};

userConnections = function(username, isBrand, followText) {
    var prof = Meteor.users.findOne({username: {$regex: new RegExp("^" + username.toLowerCase() + "$", "i")}});
    var arr = isBrand ? prof['brand' + followText] : prof['consumer' + followText];
    if (prof && arr) {
        return {_id: {$in : arr}};
    }
    return false;
};


myNetwork = function(id) {
    var prof = Meteor.users.findOne({_id: id});
    if (prof) {
        return _.union([prof._id], prof.brandFollowing ? prof.brandFollowing : [],
            prof.consumerFollowing ? prof.consumerFollowing : []);
    }
    return [];
};

inMyNetwork = function(userId, myId) {
    return _.contains(myNetwork(myId), userId);
};