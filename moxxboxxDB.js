/*
 Server functions for moxxboxx, moxx's messaging service
 */

Meteor.methods({
    //create a chat group
    createChat: function(options) {
        check(options, {
            title: String,
            eventId: String
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You need to be logged in to do this");

        if (options.eventId.length > 0)
            return MoxxBoxx.insert({
                title: options.title,
                members: [this.userId],
                eventId: options.eventId,
                currentlyTyping: []
            });
        else
            return MoxxBoxx.insert({
                title: options.title,
                members: [this.userId],
                currentlyTyping: []
            });
    },

    changeTitle: function(options) {
        check(options, {
            title: String,
            chatId: String
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You need to be logged in to do this");

        MoxxBoxx.update({_id: options.chatId, members: this.userId}, {$set: {title: options.title}});
    },

    leaveChat: function(options) {
        check(options, {
            chatId: String
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You need to be logged in to do this");
        MoxxBoxx.update(
            {_id:options.chatId},
            {$pull: {members: this.userId}}
        );
    },

    addMember: function(options) {
        check(options, {
            chatId: String,
            members: [String]
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You need to be logged in to do this");
        var members = [];
        for (var i = 0 ; i < options.members.length ; i++) {
            var profile = Meteor.users.findOne({username: {$regex: new RegExp("^" + options.members[i].toLowerCase() + "$", "i")}});
            if (profile && !_.contains(members, profile._id)) {
                members.push(profile._id);
            }
        }
        MoxxBoxx.update(
            {_id:options.chatId, members: this.userId},
            {$addToSet: {members: {$each: members}}}
        );
        addNotification(members,
            {
                notificationType: notificationTypes.addChatMember,
                users: [this.userId],
                link: "/moxxboxx/" + options.chatId,
                chatId: options.chatId
            });
    },

    writeMessage: function(options) {
        check(options, {
            chatId: String,
            message: String,
            upload: Match.OneOf(Object, Boolean)
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You need to be logged in to do this.");

        MoxxBoxx.update(
            {_id:options.chatId, members: this.userId},
            {$push: {messages: {
                user: this.userId,
                message: options.message,
                upload: options.upload ? options.upload : false,
                timeStamp: (new Date()).getTime()
            }}}
        );
    },

    currentlyTyping: function(options) {
        check(options, {
            chatId: String,
            addRemove: Boolean
        });

        if (!this.userId)
            throw new Meteor.Error(404, "You must be logged in to do this.");
        var updateQuery = {};
        if (options.addRemove)
            updateQuery["$push"] = {
                currentlyTyping: this.userId
            };
        else
            updateQuery["$pull"] = {
                currentlyTyping: this.userId
            };

        MoxxBoxx.update({_id: options.chatId, members: this.userId}, updateQuery);
        return true;
    }
});